﻿using System.Collections.Generic;
using System.Linq;
using CodeQuality.Analyzers.Modules.Style;
using CodeQuality.Analyzers.Modules.Style.Analyzers.Naming;
using CodeQuality.TestKit;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.Diagnostics;
using NUnit.Framework;

namespace CodeQuality.Analyzers.Test.Modules.Style.CQ002BooleanProperty
{
    public class Diagnostic : AnalyzerTestFixture
    {
        protected override string LanguageName => LanguageNames.CSharp;

        protected override DiagnosticAnalyzer CreateAnalyzer() => new BoolPropertyAnalyzer();

        protected override IReadOnlyCollection<MetadataReference> References => new[]
        {
            MetadataReference.CreateFromFile(typeof(object).Assembly.Location),
            MetadataReference.CreateFromFile(typeof(Enumerable).Assembly.Location),
            MetadataReference.CreateFromFile(typeof(CSharpCompilation).Assembly.Location),
            MetadataReference.CreateFromFile(typeof(Compilation).Assembly.Location),
        };

        [Test]
        public void CodeHasIncorrectBoolProperty()
        {
            var code = @"
    namespace ConsoleApplication1
    {
        class TypeName
        {   
        public bool [|Property|] { get; set; }
        }
    }";
            HasDiagnostic(code, Descriptors.CQ002BooleanProperty.Id);
        }
    }
}
