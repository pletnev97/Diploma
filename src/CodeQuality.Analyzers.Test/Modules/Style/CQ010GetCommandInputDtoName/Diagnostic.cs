﻿using System.Collections.Generic;
using System.Linq;
using CodeQuality.Analyzers.Modules.Style;
using CodeQuality.Analyzers.Modules.Style.Analyzers.Naming;
using CodeQuality.TestKit;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.Diagnostics;
using NUnit.Framework;

namespace CodeQuality.Analyzers.Test.Modules.Style.CQ010GetCommandInputDtoName
{
    public class Diagnostic : AnalyzerTestFixture
    {
        protected override string LanguageName => LanguageNames.CSharp;

        protected override DiagnosticAnalyzer CreateAnalyzer() => new CommandMethodInputTypeNameAnalyzer();

        protected override IReadOnlyCollection<MetadataReference> References => new[]
        {
            ReferenceSource.FromAssembly(typeof(object).Assembly),
            ReferenceSource.FromAssembly(typeof(Enumerable).Assembly),
            ReferenceSource.FromAssembly(typeof(CSharpCompilation).Assembly),
            ReferenceSource.FromAssembly(typeof(Compilation).Assembly),
            ReferenceSource.FromAssembly(typeof(HttpPostAttribute).Assembly),
            ReferenceSource.FromAssembly(typeof(IActionResult).Assembly)
        };

        [TestCase("[HttpPost]", "Book")]
        [TestCase("[HttpPut]", "Book")]
        [TestCase("[HttpDelete]", "Book")]
        [TestCase("[HttpPost]", "BookDto")]
        [TestCase("[HttpPut]", "BookDto")]
        [TestCase("[HttpDelete]", "BookDto")]
        public void IncorrectCommandDtoNameHasDiagnostic(string attribute, string command)
        {
            var code = @"
using Microsoft.AspNetCore.Mvc;

namespace TestAnalyzers
{
    class Program
    {
        public bool Property { get; set; }

        class Book
        {

        }

        ATTRIBUTE_PLACEHOLDER
        public IActionResult Post([|COMMAND_PLACEHOLDER|] input)
        {
            return new OkResult();
        }
    }
}"
                .Replace("ATTRIBUTE_PLACEHOLDER", attribute)
                .Replace("COMMAND_PLACEHOLDER", command);

            HasDiagnostic(code, Descriptors.CQ010GetCommandInputDtoName.Id);
        }

        [TestCase("[HttpPost]", "CreateBook")]
        [TestCase("[HttpPut]", "UpdateBook")]
        [TestCase("[HttpDelete]", "DeleteBook")]
        public void CorrectCommandDtoNameHasNoDiagnostic(string attribute, string command)
        {
            var code = @"
using Microsoft.AspNetCore.Mvc;

namespace TestAnalyzers
{
    class Program
    {
        public bool Property { get; set; }

        class CreateBook
        {

        }

        ATTRIBUTE_PLACEHOLDER
        public IActionResult Post(COMMAND_PLACEHOLDER input)
        {
            return new OkResult();
        }
    }
}"
                .Replace("ATTRIBUTE_PLACEHOLDER", attribute)
                .Replace("COMMAND_PLACEHOLDER", command);

            NoDiagnostic(code, Descriptors.CQ010GetCommandInputDtoName.Id);
        }
    }
}