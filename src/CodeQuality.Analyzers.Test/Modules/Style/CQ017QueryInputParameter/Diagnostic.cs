﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using CodeQuality.Analyzers.Modules.Style;
using CodeQuality.Analyzers.Modules.Style.Analyzers.Naming.CodeQuality.Analyzers.Modules.Style.Analyzers.Naming;
using CodeQuality.TestKit;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Diagnostics;
using NUnit.Framework;

namespace CodeQuality.Analyzers.Test.Modules.Style.CQ017QueryInputParameter
{
    public class Diagnostic : AnalyzerTestFixture
    {
        protected override string LanguageName => LanguageNames.CSharp;

        protected override DiagnosticAnalyzer CreateAnalyzer() => new QueryInputNameTypeAnalyzer();

        protected override IReadOnlyCollection<MetadataReference> References => new[]
        {
            ReferenceSource.FromAssembly(typeof(object).Assembly),
            ReferenceSource.FromAssembly(typeof(Enumerable).Assembly),
            ReferenceSource.FromAssembly(typeof(List<>).Assembly),
            ReferenceSource.FromAssembly(typeof(IEnumerable<>).Assembly),
            ReferenceSource.FromAssembly(Assembly.Load("netstandard, Version=2.0.0.0")),
            ReferenceSource.FromAssembly(Assembly.Load("Force, Version=1.0.0.0"))
        };

        [TestCase("Entity")]
        [TestCase("EntityDto")]
        [TestCase("GetEntityDto")]
        [TestCase("GetEntityItemsDto")]
        [TestCase("GetEntityDetailsDto")]
        [TestCase("EntityDetailsDto")]
        public void QueryInputHasIncorrectTypeNameHasDiagnostic(string inputTypeName)
        {
            var code = @"
using System.Collections.Generic;
using Force.Cqrs;

namespace TestAnalyzers
{
  public class Program6
    {
        public class PLACEHOLDER
        {

        }

        public class UserItem
        {

        }

        public interface A {

        }

        public class GetUserQuery : IQuery<Input, IEnumerable<UserItem>>, A
        {
            public IEnumerable<UserItem> Ask([|PLACEHOLDER|] spec) => new [] {new UserItem()};
        }
    }
}".Replace("PLACEHOLDER", inputTypeName);
            HasDiagnostic(code, Descriptors.CQ017QueryInputParameter.Id);
        }

        [TestCase("GetEntityDetails")]
        [TestCase("GetEntityItems")]
        public void QueryInputHasCorrectTypeNameNoDiagnostic(string inputTypeName)
        {
            var code = @"
using System.Collections.Generic;
using Force.Cqrs;

namespace TestAnalyzers
{
  public class Program6
    {
        public class PLACEHOLDER
        {

        }

        public class UserItem
        {

        }

        public interface A {

        }

        public class GetUserQuery : IQuery<Input, IEnumerable<UserItem>>, A
        {
            public IEnumerable<UserItem> Ask(PLACEHOLDER spec) => new [] {new UserItem()};
        }
    }
}".Replace("PLACEHOLDER", inputTypeName);
            NoDiagnostic(code, Descriptors.CQ017QueryInputParameter.Id);
        }
    }
}