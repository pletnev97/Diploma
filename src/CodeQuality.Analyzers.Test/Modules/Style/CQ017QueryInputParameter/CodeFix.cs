﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using CodeQuality.Analyzers.Modules.Style;
using CodeQuality.Analyzers.Modules.Style.Analyzers.Naming.CodeQuality.Analyzers.Modules.Style.Analyzers.Naming;
using CodeQuality.TestKit;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CodeFixes;
using Microsoft.CodeAnalysis.Diagnostics;
using NUnit.Framework;

namespace CodeQuality.Analyzers.Test.Modules.Style.CQ017QueryInputParameter
{
    class CodeFix : CodeFixTestFixture
    {
        protected override string LanguageName => LanguageNames.CSharp;

        protected override CodeFixProvider CreateProvider() => new StyleCodeFixProvider();

        protected override IReadOnlyCollection<MetadataReference> References => new[]
        {
            ReferenceSource.FromAssembly(typeof(object).Assembly),
            ReferenceSource.FromAssembly(typeof(Enumerable).Assembly),
            ReferenceSource.FromAssembly(typeof(List<>).Assembly),
            ReferenceSource.FromAssembly(typeof(IEnumerable<>).Assembly),
            ReferenceSource.FromAssembly(Assembly.Load("netstandard, Version=2.0.0.0")),
            ReferenceSource.FromAssembly(Assembly.Load("Force, Version=1.0.0.0"))
        };

        protected override IReadOnlyCollection<DiagnosticAnalyzer> CreateAdditionalAnalyzers() =>
            new[] {new QueryInputNameTypeAnalyzer()};

        [TestCase("User", "GetUserItems")]
        [TestCase("UserDto", "GetUserItems")]
        [TestCase("UserItemsDto", "GetUserItems")]
        [TestCase("GetUserItemsDto", "GetUserItems")]
        public void ReplaceNameCodeFix(string oldName, string newName)
        {
            var before = @"
using System.Collections.Generic;
using Force.Cqrs;

namespace TestAnalyzers
{
  public class Program6
    {
        public class OLD_NAME
        {

        }

        public class UserItem
        {

        }

        public class GetUserQuery : IQuery<OLD_NAME, IEnumerable<UserItem>>
        {
            public IEnumerable<UserItem> Ask([|OLD_NAME|] spec) => new [] {new UserItem()};
        }
    }
}".Replace("OLD_NAME", oldName);

            var after = @"
using System.Collections.Generic;
using Force.Cqrs;

namespace TestAnalyzers
{
  public class Program6
    {
        public class NEW_NAME
        {

        }

        public class UserItem
        {

        }

        public class GetUserQuery : IQuery<NEW_NAME, IEnumerable<UserItem>>
        {
            public IEnumerable<UserItem> Ask(NEW_NAME spec) => new [] {new UserItem()};
        }
    }
}".Replace("NEW_NAME", newName);

            TestCodeFix(before, after, Descriptors.CQ017QueryInputParameter.Id);
        }
    }
}