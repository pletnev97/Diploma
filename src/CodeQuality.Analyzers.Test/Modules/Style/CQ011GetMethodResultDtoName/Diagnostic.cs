﻿using System.Collections.Generic;
using System.Linq;
using CodeQuality.Analyzers.Modules.Style;
using CodeQuality.Analyzers.Modules.Style.Analyzers.Naming;
using CodeQuality.TestKit;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.Diagnostics;
using NUnit.Framework;

namespace CodeQuality.Analyzers.Test.Modules.Style.CQ011GetMethodResultDtoName
{
    public class Diagnostic : AnalyzerTestFixture
    {
        protected override string LanguageName => LanguageNames.CSharp;

        protected override DiagnosticAnalyzer CreateAnalyzer() => new GetCommandOutputTypeNameAnalyzer();

        protected override IReadOnlyCollection<MetadataReference> References => new[]
        {
            ReferenceSource.FromAssembly(typeof(object).Assembly),
            ReferenceSource.FromAssembly(typeof(Enumerable).Assembly),
            ReferenceSource.FromAssembly(typeof(List<>).Assembly),
            ReferenceSource.FromAssembly(typeof(IEnumerable<>).Assembly),
            ReferenceSource.FromAssembly(typeof(List).Assembly),
            ReferenceSource.FromAssembly(typeof(CSharpCompilation).Assembly),
            ReferenceSource.FromAssembly(typeof(Compilation).Assembly),
            ReferenceSource.FromAssembly(typeof(HttpPostAttribute).Assembly),
            ReferenceSource.FromAssembly(typeof(IActionResult).Assembly)
        };

        [TestCase("Book")]
        [TestCase("List<Book>")]
        [TestCase("IEnumerable<Book>")]
        [TestCase("IList<Book>")]
        [TestCase("ICollection<Book>")]
        [TestCase("IReadOnlyCollection<Book>")]
        public void ReturnTypeHasIncorrectNameHasDiagnostic(string returnType)
        {
            var code = @"
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace TestAnalyzers
{
    class Program
    {
        public bool Property { get; set; }

        class Book
        {

        }

        [HttpGet]
        public ActionResult<[|PLACEHOLDER|]> Get(int id)
        {
            return new OkResult();
        }
    }
}"
                .Replace("PLACEHOLDER", returnType);
            HasDiagnostic(code, Descriptors.CQ011GetMethodResultDtoName.Id);
        }

        [TestCase("BookDetails")]
        [TestCase("IEnumerable<BookItem>")]
        public void ReturnTypeHasCorrectNameNoDiagnostic(string dtoName)
        {
            var code = @"
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace TestAnalyzers
{
    class Program
    {
        public bool Property { get; set; }

        class BookItem
        {

        }

        [HttpGet]
        public ActionResult<PLACEHOLDER> Get(int categoryId)
        {
            return new OkResult();
        }
    }
}"
                .Replace("PLACEHOLDER", dtoName);
            NoDiagnostic(code, Descriptors.CQ011GetMethodResultDtoName.Id);
        }
    }
}
