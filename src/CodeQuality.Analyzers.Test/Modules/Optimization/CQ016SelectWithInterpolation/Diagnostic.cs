﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using CodeQuality.Analyzers.Modules.Optimization;
using CodeQuality.TestKit;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Diagnostics;
using NUnit.Framework;

namespace CodeQuality.Analyzers.Test.Modules.Optimization.CQ016SelectWithInterpolation
{
    public class Diagnostic : AnalyzerTestFixture
    {
        protected override string LanguageName => LanguageNames.CSharp;

        protected override DiagnosticAnalyzer CreateAnalyzer() => new QueryInterpolationAnalyzer();

        protected override IReadOnlyCollection<MetadataReference> References => new[]
        {
            MetadataReference.CreateFromFile(typeof(object).Assembly.Location),
            MetadataReference.CreateFromFile(typeof(Enumerable).Assembly.Location),
            MetadataReference.CreateFromFile(typeof(Queryable).Assembly.Location),
            MetadataReference.CreateFromFile(typeof(Compilation).Assembly.Location),
            MetadataReference.CreateFromFile(Assembly.Load("netstandard, Version=2.0.0.0").Location),
            MetadataReference.CreateFromFile(Assembly.Load("Microsoft.EntityFrameworkCore, Version=3.1.3").Location),
            MetadataReference.CreateFromFile(Assembly.Load("Microsoft.Bcl.AsyncInterfaces, Version=1.1.0").Location),
            MetadataReference.CreateFromFile(Assembly.Load("System.ComponentModel.TypeConverter, Version=4.2.2")
                .Location),
            MetadataReference.CreateFromFile(Assembly.Load("Automapper, Version=9.0.0").Location),
            MetadataReference.CreateFromFile(typeof(System.Linq.Expressions.Expression).Assembly.Location)
        };

        [Test]
        public void CodeHasInterpolatedExpressionInQueryHasDiagnostic()
        {
            var code = @"
    using System.Collections.Generic;
    using Microsoft.EntityFrameworkCore;
    using System.Linq;

    public class User
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public bool EmailConfirmed { get; set; }
    }

    public class ShopContext : DbContext
    {
        public DbSet<User> Users { get; set; }
    }
    
    public class Program5
    {
        public void Post(int amount, string name)
        {
            var showContext = new ShopContext();

            var users = showContext.Users;
            var userFullNames = showContext.Users.Select(x => [|$""{ x.FirstName} { x.LastName}""|]);
    }
}";
            HasDiagnostic(code, Descriptors.CQ016SelectWithInterpolation.Id);
        }

        [Test]
        public void CodeHasInterpolatedTextInMapperConfigurationHasDiagnostic()
        {
            var code = @"  
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using AutoMapper.Configuration;

    public class Program5
    {
        private class UserProfile
        {
            public string FirstName { get; set; }

            public string LastName { get; set; }
        }

        private class UserProfileDto
        {
            public string FullName { get; set; }
        }

        private class CustomProfile : Profile
        {
            public CustomProfile()
            {
                CreateMap<UserProfile, UserProfileDto>()
                        .ForMember(src => src.FullName,
                            opt => opt.MapFrom(x => [|$""{ x.FirstName} { x.LastName}""|]));
            }
        }";

            HasDiagnostic(code, Descriptors.CQ016SelectWithInterpolation.Id);
        }

        [Test]
        public void CodeHasInterpolatedTextInMappingProfileHasDiagnostic()
        {
            var code = @"  
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using AutoMapper.Configuration;

    public class Program5
    {
        private class UserProfile
        {
            public string FirstName { get; set; }

            public string LastName { get; set; }
        }

        private class UserProfileDto
        {
            public string FullName { get; set; }
        }

        private class CustomProfile : Profile
        {
            public CustomProfile()
            {
                CreateMap<UserProfile, UserProfileDto>()
                        .ForMember(src => src.FullName,
                            opt => opt.MapFrom(x => [|$""{ x.FirstName} { x.LastName}""|]));
            }
        }";

            HasDiagnostic(code, Descriptors.CQ016SelectWithInterpolation.Id);
        }
    }
}