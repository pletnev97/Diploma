﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CodeQuality.Analyzers.Modules.Optimization;
using CodeQuality.Analyzers.Modules.Optimization.DotNet.Analyzers;
using CodeQuality.TestKit;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.Diagnostics;
using NUnit.Framework;

namespace CodeQuality.Analyzers.Test.Modules.Optimization.CQ008BlockingMemberAccess
{
    public class Diagnostic : AnalyzerTestFixture
    {
        protected override string LanguageName => LanguageNames.CSharp;

        protected override DiagnosticAnalyzer CreateAnalyzer() => new BlockingTaskResultAnalyzer();

        protected override IReadOnlyCollection<MetadataReference> References => new[]
        {
            MetadataReference.CreateFromFile(typeof(object).Assembly.Location),
            MetadataReference.CreateFromFile(typeof(Enumerable).Assembly.Location),
            MetadataReference.CreateFromFile(typeof(CSharpCompilation).Assembly.Location),
            MetadataReference.CreateFromFile(typeof(Compilation).Assembly.Location),
            MetadataReference.CreateFromFile(typeof(Task).Assembly.Location)
        };

        [Test]
        public void CodeHasDangerousResultPropertyInvocationHasDiagnostic()
        {
            var code = @"
    using System.Threading.Tasks;

    public class Program2
    {
        public Task PropertyTask => new Task<int>(() => 1);
        public async Task<int> Post(int id)
        {
            var task = new Task<int>(() => 1);
            task.[|Result|];
            PropertyTask.[|Result|];
            GetTask().[|Result|];
            GetTask().[|Result|];
            GetTask().[|GetAwaiter().GetResult()|];
            return GetTask().[|Result|];
        }

        private Task<int> GetTask()
        {
            return new Task<int>(() => 1);
        }
    }";
            HasDiagnostic(code, Descriptors.CQ008BlockingMemberAccess.Id);
        }

        [Test]
        public void CodeHasAwaitKeywordNoDiagnostic()
        {
            var code = @"
    using System.Threading.Tasks;

    public class Program2
    {
        public async Task<int> Post(int id)
        {
            return await GetTask();
        }

        private Task<int> GetTask()
        {
            return new Task<int>(() => 1);
        }
    }";
            NoDiagnostic(code, Descriptors.CQ008BlockingMemberAccess.Id);
        }
    }
}
