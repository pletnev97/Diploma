﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using CodeQuality.Analyzers.Modules.Optimization;
using CodeQuality.Analyzers.Modules.Optimization.EntityFrameworkCore.Analyzers;
using CodeQuality.TestKit;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Diagnostics;
using NUnit.Framework;

namespace CodeQuality.Analyzers.Test.Modules.Optimization.CQ014DatabaseLoopRequest
{
    public class Diagnostic : AnalyzerTestFixture
    {
        protected override string LanguageName => LanguageNames.CSharp;

        protected override DiagnosticAnalyzer CreateAnalyzer() => new DatabaseLoopRequestAnalyzer();

        protected override IReadOnlyCollection<MetadataReference> References => new[]
        {
            MetadataReference.CreateFromFile(typeof(object).Assembly.Location),
            MetadataReference.CreateFromFile(typeof(Enumerable).Assembly.Location),
            MetadataReference.CreateFromFile(typeof(Queryable).Assembly.Location),
            MetadataReference.CreateFromFile(typeof(Compilation).Assembly.Location),
            MetadataReference.CreateFromFile(Assembly.Load("netstandard, Version=2.0.0.0").Location),
            MetadataReference.CreateFromFile(Assembly.Load("Microsoft.EntityFrameworkCore, Version=3.1.3").Location),
            MetadataReference.CreateFromFile(Assembly.Load("Microsoft.Bcl.AsyncInterfaces, Version=1.1.0").Location),
            MetadataReference.CreateFromFile(Assembly.Load("System.ComponentModel.TypeConverter, Version=4.2.2").Location),
            MetadataReference.CreateFromFile(typeof(System.Linq.Expressions.Expression).Assembly.Location)
        };

        [TestCase("var users = [|showContext.Users.Where(x=>x.FirstName != string.Empty).Select(x => x).FirstOrDefault(x => x.FirstName.StartsWith(name));|]")]
        [TestCase("var users =  [|showContext.Users|];")]
        [TestCase("var users = [|showContext.Users.Select(x => x)|];")]
        [TestCase("var users = [|showContext.Users.Where(x=>x.FirstName != string.Empty).Select(x => x)|];")]
        [TestCase("var users = [|GetUsers();|]")]
        public void CodeHasQueryInLoopHasDiagnostic(string expression)
        {
            var code = @"
    using System.Collections.Generic;
    using Microsoft.EntityFrameworkCore;
    using System.Linq;

    public class User
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public bool EmailConfirmed { get; set; }
    }

    public class ShopContext : DbContext
    {
        public DbSet<User> Users { get; set; }
    }

    public class Program5
    {
        public void Post(int amount, string name)
        {
            var showContext = new ShopContext();

            for (var i = 0; i < amount; i++)
            {
                PLACEHOLDER
            }
        }

        private IQueryable<User> GetUsers() {
            var showContext = new ShopContext();
            return showContext.Users;
        }
    }".Replace("PLACEHOLDER", expression);

            HasDiagnostic(code, Descriptors.CQ014DatabaseLoopRequest.Id);
        }
    }
}
