﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CodeQuality.Analyzers.Modules.Optimization;
using CodeQuality.Analyzers.Modules.Optimization.DotNet.Analyzers;
using CodeQuality.TestKit;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.Diagnostics;
using NUnit.Framework;

namespace CodeQuality.Analyzers.Test.Modules.Optimization.CQ013TaskRun
{
    public class Diagnostic : AnalyzerTestFixture
    {
        protected override string LanguageName => LanguageNames.CSharp;

        protected override DiagnosticAnalyzer CreateAnalyzer() => new TaskRunAnalyzer();

        protected override IReadOnlyCollection<MetadataReference> References => new[]
        {
            MetadataReference.CreateFromFile(typeof(object).Assembly.Location),
            MetadataReference.CreateFromFile(typeof(Enumerable).Assembly.Location),
            MetadataReference.CreateFromFile(typeof(CSharpCompilation).Assembly.Location),
            MetadataReference.CreateFromFile(typeof(Compilation).Assembly.Location),
            MetadataReference.CreateFromFile(typeof(Task).Assembly.Location)
        };

        [Test]
        public void CodeHasTaskRunInvocationHasDiagnostic()
        {
            var code = @"
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;

   public class Program4
    {
        public async Task<IActionResult> Post(int id)
        {
            return new OkObjectResult([|Task.Run(() => { })|]);
        }
    }";

            HasDiagnostic(code, Descriptors.CQ013TaskRun.Id);
        }
    }
}
