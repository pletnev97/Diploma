﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CodeQuality.Hosts.WebApi.Domain.Entities
{
    public class Author : IHasId<int>
    {
        public Author(string firstName, string lastName, DateTime birthDate)
        {
            FirstName = firstName;
            LastName = lastName;
            BirthDate = birthDate;
        }

        protected Author()
        {
        }

        [Key]
        public int Id { get; protected set; }

        public string FirstName { get; protected set; }

        public string LastName { get; protected set; }

        public DateTime BirthDate { get; protected set; }

        public ICollection<BookAuthor> BookAuthors { get; protected set; }
    }
}