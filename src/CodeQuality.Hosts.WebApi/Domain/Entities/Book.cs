﻿using CodeQuality.Hosts.WebApi.Domain.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CodeQuality.Hosts.WebApi.Domain.Entities
{
    [Entity]
    public class Book : IHasId<int>
    {
        public Book(string title, DateTime publishDate)
        {
            Title = title;
            PublishDate = publishDate;
        }

        [Key]
        public int Id { get; protected set; }

        public string Title { get; protected set; }

        public bool IsPublished { get; protected set; }

        public DateTime PublishDate { get; protected set; }

        public ICollection<BookAuthor> BookAuthors { get; protected set; } = new List<BookAuthor>();

        public void Update(string title, DateTime publishDate)
        {
            Title = title;
            PublishDate = publishDate;
        }

        public void AddAuthor(Author author)
        {
            BookAuthors.Add(new BookAuthor() {Author =  author, Book =  this});
        }
    }
}