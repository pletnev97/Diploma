﻿using System.ComponentModel.DataAnnotations;

namespace CodeQuality.Hosts.WebApi.Domain
{
    public interface IHasId<TKey>
    {
        public TKey Id { get; }
    }

    public interface IHasName
    {
        public string Name { get; set; }
    }
}
