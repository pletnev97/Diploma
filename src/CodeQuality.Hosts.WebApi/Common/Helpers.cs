﻿using System.Threading.Tasks;

namespace CodeQuality.Hosts.WebApi.Common
{
    public class Helpers
    {
        public static void DoLongRunningOperation(object target) 
            => Task.Delay(1000);
    }
}