﻿using CodeQuality.Hosts.WebApi.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace CodeQuality.Hosts.WebApi.DataAccess
{
    public class LibraryContext : DbContext
    {
        public LibraryContext(DbContextOptions<LibraryContext> options)
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BookAuthor>()
                .HasKey(ba => new { ba.BookId, ba.AuthorId });

        }

        public DbSet<Book> Books { get; protected set; }

        public DbSet<Author> Authors { get; protected set; }
    }
}
