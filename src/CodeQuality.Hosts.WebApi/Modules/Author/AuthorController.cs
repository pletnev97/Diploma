﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using CodeQuality.Hosts.WebApi.Common;
using CodeQuality.Hosts.WebApi.DataAccess;
using CodeQuality.Hosts.WebApi.Modules.Book;
using CodeQuality.Hosts.WebApi.Modules.Book.Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CodeQuality.Hosts.WebApi.Modules.Author
{
    [ApiController]
    [Route("api/[controller]")]
    public class AuthorController : ControllerBase
    {
        private readonly LibraryContext _context;
        private readonly IMapper _mapper;

        public AuthorController(
            LibraryContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        [HttpGet]
        public BookDto Get([FromRoute] int id)
        {
            return _mapper.Map<BookDto>(
                _context.Find<Domain.Entities.Book>(id)
                ?? throw new ArgumentNullException());
        }

        // Queries in a loop.
        [HttpPut]
        public async Task<IActionResult> Put(
            [FromRoute] int id,
            [FromBody] UpdateBook update)
        {
            var book = await _context.FindAsync<Domain.Entities.Book>(id);

            book.Update(update.Title, update.PublishDate);

            await _context.SaveChangesAsync();

            // Do long running operation.
            // Undesirable action.
            Task.Run(() => Helpers.DoLongRunningOperation(book));

            return Ok(id);
        }
    }
}