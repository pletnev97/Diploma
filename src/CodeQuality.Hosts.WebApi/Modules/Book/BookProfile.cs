﻿using AutoMapper;
using CodeQuality.Hosts.WebApi.Modules.Book.Dto;

namespace CodeQuality.Hosts.WebApi.Modules.Book
{
    public class BookProfile : Profile
    {
        public BookProfile()
        {
            CreateMap<Domain.Entities.Book, BookDto>()
                .ForMember(x => x.Title, src => src.MapFrom(x => x.Title))
                .ForAllOtherMembers(x => x.Ignore());
        }
    }
}