﻿namespace CodeQuality.Hosts.WebApi.Modules.Book.Dto
{
    public class GetBookByNameParts
    {
        public string FirstPart { get; set; }

        public string SecondPart { get; set; }
    }
}
