﻿namespace CodeQuality.Hosts.WebApi.Modules.Book.Dto
{
    public class GetAuthorBooksItems
    {
        public int AuthorId { get; set; }
    }
}