﻿using System;

namespace CodeQuality.Hosts.WebApi.Modules.Book.Dto
{
    public class UpdateBook
    {
        public string Title { get; set; }

        public DateTime PublishDate { get; set; }
    }
}