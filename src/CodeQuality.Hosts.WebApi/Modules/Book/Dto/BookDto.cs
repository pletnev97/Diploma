﻿using System;

namespace CodeQuality.Hosts.WebApi.Modules.Book.Dto
{
    public class BookDto
    {
        public string Title { get; set; }
        public string Text { get; set; }
    }
}
