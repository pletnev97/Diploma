﻿namespace CodeQuality.Hosts.WebApi.Modules.Book.Dto
{
    public class UpdateBookAuthors
    {
        public int AuthorId { get; set; }

        public int[] BookIds { get; set; }
    }
}