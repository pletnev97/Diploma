﻿using System;

namespace CodeQuality.Hosts.WebApi.Modules.Book.Dto
{
    public class AuthorBookItem
    {
        public string Title { get; set; }

        public bool IsPublished { get; protected set; }

        public DateTime PublishDate { get; protected set; }
    }
}