﻿namespace CodeQuality.Hosts.WebApi.Modules.Book.Queries
{
    public class GetBookDetails
    {
        public int BookId { get; set; }
    }
}
