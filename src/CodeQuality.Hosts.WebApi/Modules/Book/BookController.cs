﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CodeQuality.Hosts.WebApi.DataAccess;
using CodeQuality.Hosts.WebApi.Modules.Book.Dto;
using Force.Cqrs;
using Microsoft.AspNetCore.Mvc;

namespace CodeQuality.Hosts.WebApi.Modules.Book
{
    [ApiController] [Route("api/[controller]")]
    public class BookController : ControllerBase
    {
        private readonly LibraryContext _context;
        private readonly IHandler<UpdateBookAuthors> _bulkUpdateAuthorsHandler;
        private readonly IHandler<(int, UpdateBook), Task<int>> _updateBookHandler;
        private readonly IQuery<int, Task<BookDto>> _getBookAsyncQuery;
        private readonly IQuery<GetAuthorBooksItems, IEnumerable<AuthorBookItem>> _getAuthorBooksQuery;
        private readonly IMapper _mapper;

        public BookController(
            LibraryContext context,
            IHandler<UpdateBookAuthors> bulkUpdateAuthorsHandler,
            IHandler<(int, UpdateBook), Task<int>> updateBookHandler,
            IQuery<int, Task<BookDto>> getBookAsyncQuery,
            IQuery<GetAuthorBooksItems, IEnumerable<AuthorBookItem>> getAuthorBooksQuery,
            IMapper mapper)
        {
            _context = context;
            _bulkUpdateAuthorsHandler = bulkUpdateAuthorsHandler;
            _updateBookHandler = updateBookHandler;
            _mapper = mapper;
            _getAuthorBooksQuery = getAuthorBooksQuery;
            _getBookAsyncQuery = getBookAsyncQuery;
        }

        // Using string interpolation.
        [HttpGet("getbyname")]
        public IActionResult GetByName([FromRoute] GetBookByNameParts query)
            => Ok(_mapper.Map<BookDto>(
                _context.Books
                    .FirstOrDefault(b => b.Title == query.FirstPart + query.SecondPart)));

        //
        [HttpGet("getauthorbooks")]
        public IActionResult GetAuthorBooks([FromQuery] GetAuthorBooksItems query)
            => Ok(_getAuthorBooksQuery.Ask(query));

        [HttpGet("getasyncwithblock/{id}")]
        public async Task<IActionResult> GetAsyncWithBlock([FromRoute] int id)
            => Ok(await _getBookAsyncQuery.Ask(id));

        // Long running operation.
        [HttpPut("putandstartlongrunning/{id}")]
        public async Task<IActionResult> PutAndStartLongRunning(
            [FromRoute] int id,
            [FromBody] UpdateBook update)
            => Ok(await _updateBookHandler.Handle((id, update)));

        // Long running operation.
        // Mass update operation.
        [HttpPut("BulkUpdate")]
        public IActionResult BulkUpdate(UpdateBookAuthors command)
        {
            _bulkUpdateAuthorsHandler.Handle(command);
            return Ok();
        }
    }
}