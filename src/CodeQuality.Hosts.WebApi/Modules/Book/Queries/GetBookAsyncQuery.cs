﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CodeQuality.Hosts.WebApi.DataAccess;
using CodeQuality.Hosts.WebApi.Modules.Book.Dto;
using Force.Cqrs;
using Force.Extensions;

namespace CodeQuality.Hosts.WebApi.Modules.Book.Queries
{
    public class GetBookAsyncQuery : IQuery<int, Task<BookDto>>
    {
        private readonly LibraryContext _context;
        private readonly IMapper _mapper;

        public GetBookAsyncQuery(LibraryContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<BookDto> Ask(int id)
        {
            var result = _mapper.Map<BookDto>(
                await _context.FindAsync<Domain.Entities.Book>(id)
                ?? throw new ArgumentNullException());

            result.Text = await File.ReadAllTextAsync($"book_{id}.txt");

            return result;
        }
    }
}