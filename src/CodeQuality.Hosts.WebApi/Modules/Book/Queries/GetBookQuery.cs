﻿using System;
using System.IO;
using System.Threading.Tasks;
using AutoMapper;
using CodeQuality.Hosts.WebApi.DataAccess;
using CodeQuality.Hosts.WebApi.Modules.Book.Dto;
using Force.Cqrs;

namespace CodeQuality.Hosts.WebApi.Modules.Book.Queries
{
    public class GetBookQuery : IQuery<GetBook, BookDto>
    {
        private readonly LibraryContext _context;
        private readonly IMapper _mapper;

        public GetBookQuery(LibraryContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public BookDto Ask(GetBook spec)
        {
            var result = _mapper.Map<BookDto>(
                 _context.Find<Domain.Entities.Book>(spec.BookId)
                ?? throw new ArgumentNullException());

            result.Text =  File.ReadAllText($"book_{spec.BookId}.txt");

            return result;
        }
    }

    public class GetBook
    {
        public int BookId { get; set; }
    }
}
