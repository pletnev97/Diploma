﻿using System;
using CodeQuality.Hosts.WebApi.DataAccess;
using CodeQuality.Hosts.WebApi.Modules.Book.Dto;
using Force.Cqrs;

namespace CodeQuality.Hosts.WebApi.Modules.Book.Queries
{
    public class GetBookByPublishDateDetails
    {
        public int BookId { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
    }

    public class GetBooksInPublishTimeRange //: IQuery<GetBookByPublishDateDetails, BookDetails>
    {
        private readonly LibraryContext _context;

        public GetBooksInPublishTimeRange(LibraryContext context)
        {
            _context = context;
        }

        //public BookDetails Ask(GetBookByPublishDateDetails spec)
        //{
        //    var book = _context.Find<Domain.Entities.Book>(spec.BookId);
        //}
    }
}
