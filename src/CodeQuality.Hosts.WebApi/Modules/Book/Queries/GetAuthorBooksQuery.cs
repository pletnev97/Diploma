﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CodeQuality.Hosts.WebApi.DataAccess;
using CodeQuality.Hosts.WebApi.Modules.Book.Dto;
using Force.Cqrs;

namespace CodeQuality.Hosts.WebApi.Modules.Book.Queries
{
    public class GetAuthorBooksQuery : IQuery<GetAuthorBooksItems, IEnumerable<AuthorBookItem>>
    {
        private readonly LibraryContext _context;
        private readonly IMapper _mapper;

        public GetAuthorBooksQuery(LibraryContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public IEnumerable<AuthorBookItem> Ask(GetAuthorBooksItems spec)
        {
            var books = _context.Authors
                .Where(a => a.Id == spec.AuthorId)
                .Select(a => a.BookAuthors.Select(ba => ba.Book).ToList())
                .SelectMany(b => b)
                .ToList();


            return _mapper.Map<IEnumerable<AuthorBookItem>>(books);
        }
    }
}