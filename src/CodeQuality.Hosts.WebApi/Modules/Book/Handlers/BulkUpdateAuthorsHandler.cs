﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CodeQuality.Hosts.WebApi.DataAccess;
using CodeQuality.Hosts.WebApi.Modules.Book.Dto;
using Force.Cqrs;

namespace CodeQuality.Hosts.WebApi.Modules.Book.Handlers
{
    public class BulkUpdateAuthorsHandler : IHandler<UpdateBookAuthors>
    {
        private readonly LibraryContext _context;

        public BulkUpdateAuthorsHandler(LibraryContext context)
        {
            _context = context;
        }
        public void Handle(UpdateBookAuthors command)
        {
            var author = _context
                .Find<Domain.Entities.Author>(command.AuthorId);


           var books = _context.Books
               .Where(b => command.BookIds.Contains(b.Id))
               .ToList();

            // Changes are in memory.
                foreach (var book in books)
            {
                book.AddAuthor(author);
            }

            _context.SaveChanges();
        }
    }
}
