﻿using System;
using System.Threading.Tasks;
using CodeQuality.Hosts.WebApi.Common;
using CodeQuality.Hosts.WebApi.DataAccess;
using CodeQuality.Hosts.WebApi.Modules.Book.Dto;
using Force.Cqrs;

namespace CodeQuality.Hosts.WebApi.Modules.Book.Handlers
{
    public class UpdateBookHandler : IHandler<(int, UpdateBook), Task<int>>
    {
        private readonly LibraryContext _context;

        public UpdateBookHandler(LibraryContext context)
        {
            _context = context;
        }

        public async Task<int> Handle((int, UpdateBook) command)
        {
            var bookId = command.Item1;
            var updatedBook = command.Item2;

            var book = await _context.FindAsync<Domain.Entities.Book>(bookId);

            book.Update(updatedBook.Title, updatedBook.PublishDate);

            await _context.SaveChangesAsync();

            Helpers.DoLongRunningOperation(book);

            return bookId;
        }
    }
}
