﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CodeQuality.Hosts.WebApi.Migrations
{
    public partial class Updatebookentity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsPublished",
                table: "Books",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsPublished",
                table: "Books");
        }
    }
}
