using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using CodeQuality.Hosts.WebApi.DataAccess;
using CodeQuality.Hosts.WebApi.Modules.Book.Dto;
using CodeQuality.Hosts.WebApi.Modules.Book.Handlers;
using CodeQuality.Hosts.WebApi.Modules.Book.Queries;
using Force.Cqrs;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace CodeQuality.Hosts.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<LibraryContext>(options =>
                options.UseSqlServer(
                    "Data Source=.;Initial Catalog=LibraryContext;Integrated Security=SSPI;Connect Timeout=1000"));

            services.AddAutoMapper(typeof(Startup));

            services.AddScoped<IHandler<UpdateBookAuthors>, BulkUpdateAuthorsHandler>();
            services.AddScoped<IHandler<(int, UpdateBook), Task<int>>, UpdateBookHandler>();
            services.AddScoped<IQuery<int, Task<BookDto>>, GetBookAsyncQuery>();
            services.AddScoped<IQuery<GetAuthorBooksItems, IEnumerable<AuthorBookItem>>, GetAuthorBooksQuery>();

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}