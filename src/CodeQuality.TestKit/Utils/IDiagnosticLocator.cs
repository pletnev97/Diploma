﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Text;

namespace CodeQuality.TestKit.Utils
{
    public interface IDiagnosticLocator
    {
        bool Match(Location location);
        TextSpan GetSpan();

        string Description();
    }
}