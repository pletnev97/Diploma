﻿namespace CodeQuality.Analyzers
{
    internal static class AnalyzerCategory
    {
        internal const string Design = "CodeQualityAnalyzers.Design";
        internal const string Optimization = "CodeQualityAnalyzers.Optimization";
        internal const string Style = "CodeQualityAnalyzers.Style";
    }
}
