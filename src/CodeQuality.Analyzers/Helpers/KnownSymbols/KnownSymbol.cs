﻿using Gu.Roslyn.AnalyzerExtensions;

namespace CodeQuality.Analyzers.Helpers.KnownSymbols
{
    internal static class KnownSymbol
    {
        internal static readonly QualifiedType Void = Create("System.Void", "void");
        internal static readonly QualifiedType Object = Create("System.Object", "object");
        internal static readonly QualifiedType Boolean = Create("System.Boolean", "bool");
        internal static readonly QualifiedType Func = Create("System.Func");

        internal static readonly QualifiedType HttpPostAttribute = Create("Microsoft.AspNetCore.Mvc.HttpPostAttribute");
        internal static readonly QualifiedType HttpGetAttribute = Create("Microsoft.AspNetCore.Mvc.HttpGetAttribute");
        internal static readonly QualifiedType HttpPutAttribute = Create("Microsoft.AspNetCore.Mvc.HttpPutAttribute");
        internal static readonly QualifiedType HttpDeleteAttribute = Create("Microsoft.AspNetCore.Mvc.HttpDeleteAttribute");

        internal static readonly QualifiedType EnumerableInterface = Create("System.Collections.IEnumerable");
        internal static readonly QualifiedType GenericQueryableInterface = Create("System.Linq.IQueryable`1");
        internal static readonly QualifiedType QueryableInterface = Create("System.Linq.IQueryable");
        internal static readonly QualifiedType Queryable = Create("System.Linq.Queryable");

        internal static readonly QualifiedType MemberConfigurationExpressionInterface = Create("AutoMapper.IMemberConfigurationExpression`3");

        internal static readonly GenericQueryType GenericQueryInterface = new GenericQueryType();

        internal static readonly TaskAwaiterType TaskAwaiter = new TaskAwaiterType();

        internal static readonly GenericTaskType GenericTaskType = new GenericTaskType();
        internal static readonly TaskType TaskType = new TaskType();

        private static QualifiedType Create(string qualifiedName, string? alias = null)
        {
            return new QualifiedType(qualifiedName, alias);
        }
    }
}
