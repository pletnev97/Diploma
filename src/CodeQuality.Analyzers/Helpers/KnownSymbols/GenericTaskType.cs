﻿using Gu.Roslyn.AnalyzerExtensions;

namespace CodeQuality.Analyzers.Helpers.KnownSymbols
{
    internal class GenericTaskType : QualifiedType
    {
        internal readonly QualifiedMethod FromResult;
        internal readonly QualifiedMethod ConfigureAwait;
        internal readonly QualifiedMethod Run;
        internal readonly QualifiedProperty CompletedTask;
        internal readonly QualifiedProperty Result;

        internal GenericTaskType()
            : base("System.Threading.Tasks.Task`1")
        {
            FromResult = new QualifiedMethod(this, nameof(FromResult));
            Run = new QualifiedMethod(this, nameof(Run));
            ConfigureAwait = new QualifiedMethod(this, nameof(ConfigureAwait));
            CompletedTask = new QualifiedProperty(this, nameof(CompletedTask));
            Result = new QualifiedProperty(this, nameof(Result));
        }
    }

    internal class TaskType : QualifiedType
    {
        internal readonly QualifiedMethod Run;

        internal TaskType()
            : base("System.Threading.Tasks.Task")
        {
            Run = new QualifiedMethod(this, nameof(Run));
        }
    }
}