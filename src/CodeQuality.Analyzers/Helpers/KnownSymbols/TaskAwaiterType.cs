﻿using Gu.Roslyn.AnalyzerExtensions;

namespace CodeQuality.Analyzers.Helpers.KnownSymbols
{
    internal class TaskAwaiterType : QualifiedType
    {
        internal readonly QualifiedMethod GetResult;

        internal TaskAwaiterType()
            : base("System.Runtime.CompilerServices.TaskAwaiter`1")
        {
            GetResult = new QualifiedMethod(this, nameof(GetResult));
        }
    }
}
