﻿using Gu.Roslyn.AnalyzerExtensions;

namespace CodeQuality.Analyzers.Helpers.KnownSymbols
{
    internal class GenericQueryType : QualifiedType
    {
        internal readonly QualifiedMethod Ask;

        internal GenericQueryType() : base("Force.Cqrs.IQuery`2")
        {
            Ask = new QualifiedMethod(this, "Ask");
        }
    }
}