﻿using Microsoft.CodeAnalysis;

namespace CodeQuality.Analyzers.Helpers
{
    internal static class DiagnosticDescriptorHelper
    {
        internal static DiagnosticDescriptor Create(
            string id,
            string title,
            string messageFormat,
            string category,
            DiagnosticSeverity defaultSeverity,
            bool isEnabledByDefault,
            string description,
            params string[] customTags)
            => new DiagnosticDescriptor(
                id: id,
                title: title,
                messageFormat: messageFormat,
                category: category,
                defaultSeverity: defaultSeverity,
                isEnabledByDefault: isEnabledByDefault,
                description: description,
                customTags: customTags);
    }
}