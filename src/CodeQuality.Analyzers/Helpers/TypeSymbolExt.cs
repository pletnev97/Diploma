﻿using Gu.Roslyn.AnalyzerExtensions;
using Microsoft.CodeAnalysis;

namespace CodeQuality.Analyzers.Helpers
{
    public static class TypeSymbolExt
    {
        public static bool TryFindInterface(this ITypeSymbol typeSymbol, QualifiedType interfaceType,
            Compilation compilation,
            out ISymbol? resultSymbol)
        {
            resultSymbol = default;

            return typeSymbol.Interfaces.OfType<INamedTypeSymbol>().TryFirst(
#pragma warning disable CS8600 // Converting null literal or possible null value to non-nullable type.
                i => ((INamedTypeSymbol)i)!.ConstructedFrom.IsAssignableTo(interfaceType, compilation),
#pragma warning restore CS8600 // Converting null literal or possible null value to non-nullable type.
                out resultSymbol);
        }
    }
}