﻿using System.Collections.Immutable;
using System.Composition;
using System.Linq;
using CodeQuality.Analyzers.Modules.Optimization.Walkers;
using Gu.Roslyn.AnalyzerExtensions;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace CodeQuality.Analyzers.Modules.Optimization.EntityFrameworkCore.Analyzers
{
    [DiagnosticAnalyzer(LanguageNames.CSharp), Shared]
    // ReSharper disable once UnusedMember.Global
    public class DatabaseLoopRequestAnalyzer : DiagnosticAnalyzer
    {
        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics
            => ImmutableArray.Create(Descriptors.CQ014DatabaseLoopRequest);

        public override void Initialize(AnalysisContext context)
        {
            context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.Analyze | GeneratedCodeAnalysisFlags.ReportDiagnostics);
            context.EnableConcurrentExecution();
            context.RegisterSyntaxNodeAction(Handle, SyntaxKind.ForEachStatement, SyntaxKind.ForStatement,
                SyntaxKind.WhileStatement);
        }

        private static void Handle(SyntaxNodeAnalysisContext context)
        {
            var semanticModel = context.SemanticModel;

            if (context.IsExcludedFromAnalysis() || !CheckIsLoopStatementNode(context.Node)) return;

            var blockStatement = ((StatementSyntax) context.Node).ChildNodes().OfType<BlockSyntax>().First();

            var expressions = QueryableMethodsWalker.Visit(blockStatement, semanticModel).Expressions;

            if (expressions.Any())
            {
                expressions.ForEach(e =>
                    context.ReportDiagnostic(Diagnostic.Create(Descriptors.CQ014DatabaseLoopRequest,
                        e.GetLocation())));
            }
        }

        private static bool CheckIsLoopStatementNode(SyntaxNode node) => node switch
        {
            ForEachStatementSyntax _ => true,
            ForStatementSyntax _ => true,
            WhileStatementSyntax _ => true,
            _ => false
        };
    }
}