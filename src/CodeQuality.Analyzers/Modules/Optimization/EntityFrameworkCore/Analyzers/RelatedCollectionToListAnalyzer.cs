﻿using System.Collections.Immutable;
using System.Composition;
using System.Linq;
using CodeQuality.Analyzers.Modules.Optimization.Walkers;
using Gu.Roslyn.AnalyzerExtensions;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace CodeQuality.Analyzers.Modules.Optimization.EntityFrameworkCore.Analyzers
{
    [DiagnosticAnalyzer(LanguageNames.CSharp), Shared]
    // ReSharper disable once UnusedMember.Global
    public class RelatedCollectionToListAnalyzer : DiagnosticAnalyzer
    {
        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics
            => ImmutableArray.Create(Descriptors.CQ25AppealToRelatedCollection);

        public override void Initialize(AnalysisContext context)
        {
            context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.Analyze | GeneratedCodeAnalysisFlags.ReportDiagnostics);
            context.EnableConcurrentExecution();
            context.RegisterSyntaxNodeAction(Handle, SyntaxKind.InvocationExpression);
        }

        private static void Handle(SyntaxNodeAnalysisContext context)
        {
            var semanticModel = context.SemanticModel;
            var invocationExpression = context.Node as InvocationExpressionSyntax;
            var isDbRequest = GetIsSelectInsideExpression(semanticModel, invocationExpression);

            if (isDbRequest)
            {
                var expressionError = CheckCondition(invocationExpression);

                if (expressionError != null)
                {
                    var token = expressionError.DescendantTokens()
                        .FirstOrDefault(t => t.ValueText == "Select" || t.ValueText == "Where");

                    var diagnostic = Diagnostic.Create(
                        Descriptors.CQ25AppealToRelatedCollection,
                        token.GetLocation(),
                        token.Text
                    );

                    context.ReportDiagnostic(diagnostic);
                }
            }
        }

        private static bool GetIsSelectInsideExpression(SemanticModel semanticModel, SyntaxNode node)
        {
            return node.DescendantNodes().Any(subNode =>
            {
                var symbol = semanticModel.GetSymbolInfo(subNode);
                return symbol.Symbol != null && symbol.Symbol.ContainingSymbol.ToString() == "System.Linq.Queryable"
                     && (symbol.Symbol.Name == "Select" || symbol.Symbol.Name == "Where" || symbol.Symbol.Name == "FirstOrDefault");
            });
        }

        private static SyntaxNode CheckCondition(InvocationExpressionSyntax ies)
        {
            var argumentList = ies.ChildNodes().FirstOrDefault(node => node.IsKind(SyntaxKind.ArgumentList));
            if (argumentList == null) return null;

            return argumentList
                .DescendantNodes()
                .Where(node => node.IsKind(SyntaxKind.SimpleMemberAccessExpression) && node.TryGetInferredMemberName() == "Select")
                .FirstOrDefault(node =>
                {
                    var parent = GetParentSimpleMemberAccessExpression(node);
                    return !(parent != null
                            && parent.IsKind(SyntaxKind.SimpleMemberAccessExpression)
                            && parent.TryGetInferredMemberName() == "ToList");
                });
        }

        private static SyntaxNode GetParentSimpleMemberAccessExpression(SyntaxNode node)
        {
            return node.Parent.Parent;
        }

        private static SyntaxNode GetSimpleLambaExpression(SyntaxNode node)
        {
            return node.DescendantNodes().FirstOrDefault(n => n.IsKind(SyntaxKind.SimpleLambdaExpression));
        }
    }
}