//using System.Collections.Immutable;
//using System.Composition;
//using System.Linq;
//using System.Threading.Tasks;
//using Microsoft.CodeAnalysis;
//using Microsoft.CodeAnalysis.CodeActions;
//using Microsoft.CodeAnalysis.CodeFixes;
//using Roslyn.Analyzers.Modules.Design.Analyzers;
//using Roslyn.Analyzers.Modules.Style.Analyzers;

//namespace Roslyn.Analyzers.Modules.Optimization
//{
//    [ExportCodeFixProvider(LanguageNames.CSharp, Name = nameof(CodeFix)), Shared]
//    public class CodeFix : CodeFixProvider
//    {
//        public sealed override ImmutableArray<string> FixableDiagnosticIds =>
//             ImmutableArray.Create();

//        public sealed override Task RegisterCodeFixesAsync(CodeFixContext context)
//        {
//            var diagnostic = context.Diagnostics.First();


//            if (diagnostic.Id == Style.Descriptors.CQ003BooleanVariable.Id)
//                context.RegisterCodeFix(
//                    CodeAction.Create(
//                        title: "Add prefix \"is\".",
//                        createChangedSolution: c => BoolVariableAnalyzer.CodeFix(context.Document, context, c),
//                        equivalenceKey: "Add prefix \"is\"."),
//                    diagnostic);
//            else if (diagnostic.Id == Style.Descriptors.CQ002BooleanProperty.Id)
//                context.RegisterCodeFix(
//                    CodeAction.Create(
//                        title: "Add prefix \"Is\".",
//                        c => BoolPropertyAnalyzer.CodeFix(context.Document, context, c),
//                        equivalenceKey: "Add prefix \"Is\"."),
//                    diagnostic);


//            return Task.CompletedTask;
//        }
//    }
//}
