﻿using System.Collections.Immutable;
using System.Composition;
using System.Linq;
using CodeQuality.Analyzers.Modules.Optimization.Walkers;
using Gu.Roslyn.AnalyzerExtensions;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace CodeQuality.Analyzers.Modules.Optimization.DotNet.Analyzers
{
    [DiagnosticAnalyzer(LanguageNames.CSharp), Shared]
    // ReSharper disable once UnusedMember.Global
    public class BlockingTaskResultAnalyzer : DiagnosticAnalyzer
    {
        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics
            => ImmutableArray.Create(Descriptors.CQ008BlockingMemberAccess);

        public override void Initialize(AnalysisContext context)
        {
            context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.Analyze | GeneratedCodeAnalysisFlags.ReportDiagnostics);
            context.EnableConcurrentExecution();
            context.RegisterSyntaxNodeAction(Handle, SyntaxKind.MethodDeclaration);
        }

        private static void Handle(SyntaxNodeAnalysisContext context)
        {
            var semanticModel = context.SemanticModel;

            var a = semanticModel.Compilation.GetDiagnostics();

            if (!context.IsExcludedFromAnalysis() &&
                context.Node is MethodDeclarationSyntax methodNode &&
                semanticModel.GetDeclaredSymbol(methodNode) is { } methodSymbol &&
                methodSymbol.IsAsync)
            {
                var taskPropMemberAccessExpressions =
                    TaskBlockOperationsMethodAccessWalker.Visit(methodNode, semanticModel).Expressions;

                if (taskPropMemberAccessExpressions.Any())
                {
                    taskPropMemberAccessExpressions.ForEach(e =>
                        context.ReportDiagnostic(Diagnostic.Create(Descriptors.CQ008BlockingMemberAccess,
                            e.GetLocation())));
                }
            }
        }
    }
}