﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using CodeQuality.Analyzers.Helpers.KnownSymbols;
using Gu.Roslyn.AnalyzerExtensions;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace CodeQuality.Analyzers.Modules.Optimization
{
    [DiagnosticAnalyzer(LanguageNames.CSharp)]
    public class QueryInterpolationAnalyzer : DiagnosticAnalyzer
    {
        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics
            => ImmutableArray.Create(Descriptors.CQ016SelectWithInterpolation);

        public override void Initialize(AnalysisContext context)
        {
            context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.Analyze |
                                                   GeneratedCodeAnalysisFlags.ReportDiagnostics);
            context.EnableConcurrentExecution();
            context.RegisterSyntaxNodeAction(Handle, SyntaxKind.InvocationExpression);
        }

        private static void Handle(SyntaxNodeAnalysisContext context)
        {
            if (!context.IsExcludedFromAnalysis() &&
                context.Node is InvocationExpressionSyntax invocationExpressionNode &&
                context.SemanticModel.TryGetSymbol(invocationExpressionNode, context.CancellationToken,
                    out var methodSymbol) &&
                (methodSymbol.ContainingType == KnownSymbol.Queryable ||
                 methodSymbol.ContainingType == KnownSymbol.MemberConfigurationExpressionInterface) &&
                invocationExpressionNode.ArgumentList.Arguments.Count > 0)

                GetInterpolationSyntaxNodes(invocationExpressionNode)
                    .ForEach(isn =>
                        context.ReportDiagnostic(Diagnostic.Create(Descriptors.CQ016SelectWithInterpolation,
                            isn.GetLocation())));
        }

        private static List<InterpolatedStringExpressionSyntax>
            GetInterpolationSyntaxNodes(InvocationExpressionSyntax node) =>
            node.ArgumentList.Arguments.SelectMany(a =>
                    a.Expression.DescendantNodes().OfType<InterpolatedStringExpressionSyntax>())
                .ToList();
    }
}