using System.Diagnostics.CodeAnalysis;
using CodeQuality.Analyzers.Helpers;
using Microsoft.CodeAnalysis;

namespace CodeQuality.Analyzers.Modules.Optimization
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public static class Descriptors
    {
        public static readonly DiagnosticDescriptor CQ008BlockingMemberAccess = DiagnosticDescriptorHelper.Create(
            id: "CQ008",
            title: "Dangerous invocation of \"Result\" property.",
            messageFormat: "Async method should have \"await\" modificator.",
            category: AnalyzerCategory.Optimization,
            defaultSeverity: DiagnosticSeverity.Warning,
            isEnabledByDefault: true,
            description: "Async method should have \"await\" modificator.");

        public static readonly DiagnosticDescriptor CQ013TaskRun = DiagnosticDescriptorHelper.Create(
            id: "CQ013",
            title: "Inefficient invocation\"Task.Run\" Method.",
            messageFormat: "Async methods shouldn't have \"Task.Run\" invocation.",
            category: AnalyzerCategory.Optimization,
            defaultSeverity: DiagnosticSeverity.Warning,
            isEnabledByDefault: true,
            description: "Async methods shouldn't have \"Task.Run\" invocation.");

        public static readonly DiagnosticDescriptor CQ014DatabaseLoopRequest = DiagnosticDescriptorHelper.Create(
            id: "CQ014",
            title: "Possible loop requests.",
            messageFormat: "Should use \".ToList\" to queryable and pass result to the cycle.",
            category: AnalyzerCategory.Optimization,
            defaultSeverity: DiagnosticSeverity.Warning,
            isEnabledByDefault: true,
            description: "Should use \".ToList\" to queryable and pass result to the cycle.");

        public static readonly DiagnosticDescriptor CQ016SelectWithInterpolation = DiagnosticDescriptorHelper.Create(
            id: "CQ016",
            title: "Interpolation request.",
            messageFormat: "EF core can't translate interpolated strings.",
            category: AnalyzerCategory.Optimization,
            defaultSeverity: DiagnosticSeverity.Warning,
            isEnabledByDefault: true,
            description: "EF core can't translate interpolated strings.");

        public static readonly DiagnosticDescriptor CQ25AppealToRelatedCollection = DiagnosticDescriptorHelper.Create(
    id: "CQ025",
    title: "N+1 requests.",
    messageFormat: @"Need to add "".ToList"" method for prevent N+1 problem.",
    category: AnalyzerCategory.Optimization,
    defaultSeverity: DiagnosticSeverity.Warning,
    isEnabledByDefault: true,
    description: @"Need to add "".ToList"" method for prevent N+1 problem.");
    }
}