﻿using System.Collections.Generic;
using CodeQuality.Analyzers.Helpers.KnownSymbols;
using Gu.Roslyn.AnalyzerExtensions;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace CodeQuality.Analyzers.Modules.Optimization.Walkers
{
    public class TaskRunInvocationWalker : ExecutionWalker<TaskRunInvocationWalker>
    {
        private readonly SemanticModel _semanticModel;

        public List<MemberAccessExpressionSyntax> Expressions { get; }

        private TaskRunInvocationWalker(SemanticModel semanticModel)
        {
            _semanticModel = semanticModel;
            Expressions = new List<MemberAccessExpressionSyntax>();
        }

        public override void VisitMemberAccessExpression(MemberAccessExpressionSyntax node)
        {
            if (_semanticModel.TryGetSymbol(node, default, out var symbol) &&
                symbol is IMethodSymbol &&
                symbol == KnownSymbol.TaskType.Run)
            {
                Expressions.Add(node);
            }
        }

        public static TaskRunInvocationWalker Visit(MethodDeclarationSyntax node,
            SemanticModel semanticModel) =>
            BorrowAndVisit(node, () => new TaskRunInvocationWalker(semanticModel));
    }
}