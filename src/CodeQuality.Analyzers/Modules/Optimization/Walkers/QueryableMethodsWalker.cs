﻿using System.Collections.Generic;
using CodeQuality.Analyzers.Helpers.KnownSymbols;
using Gu.Roslyn.AnalyzerExtensions;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace CodeQuality.Analyzers.Modules.Optimization.Walkers
{
    public class QueryableMethodsWalker : ExecutionWalker<QueryableMethodsWalker>
    {
        private readonly SemanticModel _semanticModel;

        public List<ExpressionSyntax> Expressions { get; }

        private QueryableMethodsWalker(SemanticModel semanticModel)
        {
            _semanticModel = semanticModel;
            Expressions = new List<ExpressionSyntax>();
        }

        public override void VisitMemberAccessExpression(MemberAccessExpressionSyntax node) => ResolveNode(node);

        public override void VisitInvocationExpression(InvocationExpressionSyntax node) => ResolveNode(node);

        public static QueryableMethodsWalker Visit(BlockSyntax node,
            SemanticModel semanticModel) =>
            BorrowAndVisit(node, () => new QueryableMethodsWalker(semanticModel));

        private void ResolveNode(ExpressionSyntax node)
        {
            if (TryGetSymbol(node, out var symbol) && CheckIsQuerySymbol(symbol) &&
                !(TryGetSymbol(node.Parent, out var parentSymbol) && CheckIsQuerySymbol(parentSymbol)))
            {
                Expressions.Add(node);
            }
        }

        private bool CheckIsQuerySymbol(ISymbol symbol) => symbol switch
        {
            IMethodSymbol methodSymbol when methodSymbol.ContainingType == KnownSymbol.Queryable ||
                                            (methodSymbol.IsExtensionMethod && methodSymbol.ReceiverType ==
                                                KnownSymbol.GenericQueryableInterface) ||
                                            methodSymbol.ReturnType == KnownSymbol.GenericQueryableInterface => true,
            IPropertySymbol propertySymbol when propertySymbol.Type.IsAssignableToEither(KnownSymbol.GenericQueryableInterface,
                KnownSymbol.QueryableInterface,
                _semanticModel.Compilation) => true,
            IFieldSymbol propertySymbol when propertySymbol.Type.IsAssignableToEither(KnownSymbol.GenericQueryableInterface,
                KnownSymbol.QueryableInterface,
                _semanticModel.Compilation) => true,
            _ => false
        };

        private bool TryGetSymbol(SyntaxNode node, out ISymbol symbol)
        {
            switch (node)
            {
                case MemberAccessExpressionSyntax memberAccessExpression when _semanticModel.TryGetSymbol(
                    memberAccessExpression, default, out var memberAccSymbol):
                    symbol = memberAccSymbol;
                    return true;
                case InvocationExpressionSyntax invocationExpression when _semanticModel.TryGetSymbol(
                    invocationExpression, default, out var memberAccSymbol):
                    symbol = memberAccSymbol;
                    return true;
                default:
                    symbol = default;
                    return false;
            }
        }
    }
}