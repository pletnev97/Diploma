﻿using System.Collections.Generic;
using CodeQuality.Analyzers.Helpers.KnownSymbols;
using Gu.Roslyn.AnalyzerExtensions;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace CodeQuality.Analyzers.Modules.Optimization.Walkers
{
    public class TaskBlockOperationsMethodAccessWalker : ExecutionWalker<TaskBlockOperationsMethodAccessWalker>
    {
        private readonly SemanticModel _semanticModel;

        public List<MemberAccessExpressionSyntax> Expressions { get; }

        private TaskBlockOperationsMethodAccessWalker(SemanticModel semanticModel)
        {
            _semanticModel = semanticModel;
            Expressions = new List<MemberAccessExpressionSyntax>();
        }

        public override void VisitMemberAccessExpression(MemberAccessExpressionSyntax node)
        {
            if (!_semanticModel.TryGetSymbol(node, default, out var symbol)) return;

            switch (symbol)
            {
                case IPropertySymbol propertySymbol
                    when propertySymbol == KnownSymbol.GenericTaskType.Result:
                    Expressions.Add(node);
                    break;
                case IMethodSymbol methodSymbol
                    when methodSymbol == KnownSymbol.TaskAwaiter.GetResult:
                    Expressions.Add(node);
                    break;
            }
        }

        public static TaskBlockOperationsMethodAccessWalker Visit(MethodDeclarationSyntax node,
            SemanticModel semanticModel) =>
            BorrowAndVisit(node, () => new TaskBlockOperationsMethodAccessWalker(semanticModel));
    }
}