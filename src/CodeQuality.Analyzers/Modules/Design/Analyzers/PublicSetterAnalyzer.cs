﻿using System.Collections.Immutable;
using System.Composition;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace CodeQuality.Analyzers.Modules.Design.Analyzers
{
    [DiagnosticAnalyzer(LanguageNames.CSharp), Shared]
    // ReSharper disable once UnusedMember.Global
    public class PublicSetterAnalyzer : DiagnosticAnalyzer
    {
        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics
            => ImmutableArray.Create(Descriptors.CQ001PublicSetter);

        public override void Initialize(AnalysisContext context)
        {
            context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.Analyze | GeneratedCodeAnalysisFlags.ReportDiagnostics);
            context.EnableConcurrentExecution();
            context.RegisterSyntaxNodeAction(Handle, SyntaxKind.PropertyDeclaration);
        }

        private static void Handle(SyntaxNodeAnalysisContext context)
        {
            var propertyNode = (PropertyDeclarationSyntax)context.Node;

            if (!propertyNode.Modifiers.Any(m => m.IsKind(SyntaxKind.PublicKeyword))) return;

            var clsNode = propertyNode
                .Ancestors()
                .OfType<ClassDeclarationSyntax>()
                .FirstOrDefault();

            if (clsNode == null) return;

            var attrs = clsNode
                .AttributeLists
                .SelectMany(x => x.Attributes);

            var semanticModel = context.SemanticModel;

            var hasValidEntityAttr = false;
            foreach (var attr in attrs)
            {
                var hasEntityAttr = attr.Name.ToString() == "Entity" || attr.Name.ToString() == "EntityAttribute";
                var isMtmEntity = attr.ArgumentList?
                    .Arguments.Any(a => a.Expression.IsKind(SyntaxKind.TrueLiteralExpression)) ?? false;

                if (!hasEntityAttr || isMtmEntity) continue;

                hasValidEntityAttr = true;
                break;
            }

            var setAccessor =
                propertyNode.AccessorList.Accessors.FirstOrDefault(acc =>
                    acc.IsKind(SyntaxKind.SetAccessorDeclaration));

            if (setAccessor != null && (!setAccessor.Modifiers.Any()
                                        || setAccessor.Modifiers.Any(m => m.IsKind(SyntaxKind.PublicKeyword))) && hasValidEntityAttr)
            {
                context.ReportDiagnostic(Diagnostic.Create(Descriptors.CQ001PublicSetter, propertyNode.Identifier.GetLocation()));
            }
        }
    }
}
