﻿using System.Collections.Immutable;
using System.Composition;
using System.Linq;
using Gu.Roslyn.AnalyzerExtensions;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace CodeQuality.Analyzers.Modules.Design.Analyzers
{
    [DiagnosticAnalyzer(LanguageNames.CSharp), Shared]
    // ReSharper disable once UnusedMember.Global
    public class EntityConstructorAnalyzer : DiagnosticAnalyzer
    {
        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics
            => ImmutableArray.Create(Descriptors.CQ021EntityConstructor);

        public override void Initialize(AnalysisContext context)
        {
            context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.Analyze |
                                                   GeneratedCodeAnalysisFlags.ReportDiagnostics);
            context.EnableConcurrentExecution();
            context.RegisterSyntaxNodeAction(Handle, SyntaxKind.ClassDeclaration);
        }

        private static void Handle(SyntaxNodeAnalysisContext context)
        {
            if (!context.IsExcludedFromAnalysis()
                && context.Node is ClassDeclarationSyntax classDeclaration
                && classDeclaration.AttributeLists.SelectMany(al => al.Attributes)
                    .Any(a => a.Name.ToString() == "Entity" || a.Name.ToString() == "EntityAttribute")
                && !classDeclaration
                    .DescendantNodes()
                    .Any(t => t.IsKind(SyntaxKind.ConstructorDeclaration))
                && classDeclaration.DescendantNodes().Any(n =>
                    n is PropertyDeclarationSyntax propertyDeclaration &&
                    propertyDeclaration.Identifier.Text != "Id"))
            {
                context.ReportDiagnostic(
                    Diagnostic.Create(Descriptors.CQ021EntityConstructor,
                        classDeclaration.Identifier.GetLocation()));
            }
        }
    }
}