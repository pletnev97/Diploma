﻿using System.Collections.Immutable;
using System.Composition;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace CodeQuality.Analyzers.Modules.Design.Analyzers
{
    [DiagnosticAnalyzer(LanguageNames.CSharp), Shared]
    // ReSharper disable once UnusedMember.Global
    public class LargeMethodAnalyzer : DiagnosticAnalyzer
    {
        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics
            => ImmutableArray.Create(Descriptors.CQ019LargeMethod);

        public override void Initialize(AnalysisContext context)
        {
            context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.Analyze |
                                                   GeneratedCodeAnalysisFlags.ReportDiagnostics);
            context.EnableConcurrentExecution();
            context.RegisterSyntaxNodeAction(Handle, SyntaxKind.MethodDeclaration);
        }

        private static int totalCount = 0;

        private static void GetContInDeep(SyntaxNode node, SyntaxKind syntaxKind)
        {
            node.ChildNodes().All(t =>
            {
                GetContInDeep(t, syntaxKind);
                return true;
            });

            node.ChildTokens().All(t =>
            {
                var count = t
                    .GetAllTrivia()
                    .Count(t2 => t2.IsKind(SyntaxKind.EndOfLineTrivia));

                totalCount += count;

                return true;
            });
        }

        private static void Handle(SyntaxNodeAnalysisContext context)
        {
            var methodDeclaration = context.Node as MethodDeclarationSyntax;

            var methodBlock = ((MemberDeclarationSyntax) context.Node).ChildNodes()
                .FirstOrDefault(n => n.IsKind(SyntaxKind.Block));

            if (methodBlock == null) return;

            totalCount = 0;

            GetContInDeep(methodBlock, SyntaxKind.EndOfLineTrivia);

            if (totalCount <= 30) return;

            var diagnostic = Diagnostic.Create(
                Descriptors.CQ019LargeMethod,
                methodDeclaration.GetLocation(),
                methodDeclaration.GetText(),
                totalCount
            );

            context.ReportDiagnostic(diagnostic);
        }
    }
}