﻿using System.Diagnostics.CodeAnalysis;
using CodeQuality.Analyzers.Helpers;
using Microsoft.CodeAnalysis;

namespace CodeQuality.Analyzers.Modules.Design
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public static class Descriptors
    {
        internal static readonly DiagnosticDescriptor CQ001PublicSetter = DiagnosticDescriptorHelper.Create(
            id: "CQ001",
            title: "\"Public\" modificator on setter.",
            messageFormat: "Setter should have \"protected\" modificator.",
            category: AnalyzerCategory.Design,
            defaultSeverity: DiagnosticSeverity.Error,
            isEnabledByDefault: true,
            description: "Setter should have \"protected\" modificator.");

        internal static readonly DiagnosticDescriptor CQ019LargeMethod = DiagnosticDescriptorHelper.Create(
            id: "CQ019",
            title: "Method is too large.",
            messageFormat: "The logic of the method should be divided.",
            category: AnalyzerCategory.Design,
            defaultSeverity: DiagnosticSeverity.Warning,
            isEnabledByDefault: true,
            description: "The logic of the method should be divided.");

        internal static readonly DiagnosticDescriptor CQ020ParameterObject = DiagnosticDescriptorHelper.Create(
            id: "CQ020",
            title: "It's may be parameter object.",
            messageFormat: "Should determine it's parameter object or not.",
            category: AnalyzerCategory.Design,
            defaultSeverity: DiagnosticSeverity.Warning,
            isEnabledByDefault: true,
            description: "Should determine it's parameter object or not.");

        internal static readonly DiagnosticDescriptor CQ021EntityConstructor = DiagnosticDescriptorHelper.Create(
            id: "CQ021",
            title: "Entity constructor.",
            messageFormat: "Entity have to have public constructor.",
            category: AnalyzerCategory.Design,
            defaultSeverity: DiagnosticSeverity.Error,
            isEnabledByDefault: true,
            description: "Entity have to public constructor.");

        private static DiagnosticDescriptor Create(
            string id,
            string title,
            string messageFormat,
            string category,
            DiagnosticSeverity defaultSeverity,
            bool isEnabledByDefault,
            string description,
            params string[] customTags)
        {
            return new DiagnosticDescriptor(
                id: id,
                title: title,
                messageFormat: messageFormat,
                category: category,
                defaultSeverity: defaultSeverity,
                isEnabledByDefault: isEnabledByDefault,
                description: description,
                customTags: customTags);
        }
    }
}
