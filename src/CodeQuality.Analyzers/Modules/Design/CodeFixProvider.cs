using System.Collections.Immutable;
using System.Composition;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CodeActions;
using Microsoft.CodeAnalysis.CodeFixes;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace CodeQuality.Analyzers.Modules.Design
{
    [ExportCodeFixProvider(LanguageNames.CSharp, Name = nameof(CodeFixProvider)), Shared]
    public class CodeFixProvider : Microsoft.CodeAnalysis.CodeFixes.CodeFixProvider
    {
        public sealed override ImmutableArray<string> FixableDiagnosticIds =>
            ImmutableArray.Create(Descriptors.CQ001PublicSetter.Id);

        public sealed override Task RegisterCodeFixesAsync(CodeFixContext context)
        {
            foreach (var diagnostic in context.Diagnostics)
            {
                context.RegisterCodeFix(
                    CodeAction.Create(
                        title: "Make \"protected\" modificator.",
                        createChangedDocument: c => FixPublicProperty(context.Document, context, c),
                        equivalenceKey: "Make \"protected\" modificator."),
                    diagnostic);
            }

            return Task.CompletedTask;
        }

        private static async Task<Document> FixPublicProperty(Document document, CodeFixContext context, CancellationToken cancellationToken)
        {
            var root = await context.Document
                .GetSyntaxRootAsync(context.CancellationToken)
                .ConfigureAwait(false);

            var diagnostic = context.Diagnostics.First();
            var diagnosticSpan = diagnostic.Location.SourceSpan;

            var property = (PropertyDeclarationSyntax)root.FindNode(diagnosticSpan);
            var setAccessor = property.AccessorList.Accessors
                .FirstOrDefault(acc => acc.IsKind(SyntaxKind.SetAccessorDeclaration));

            var protectedModifier = SyntaxFactory.Token(SyntaxKind.ProtectedKeyword);

            var modified = setAccessor.WithModifiers(SyntaxFactory.TokenList(protectedModifier));
            var newRoot = root.ReplaceNode(setAccessor, modified);

            return document.WithSyntaxRoot(newRoot);
        }
    }
}
