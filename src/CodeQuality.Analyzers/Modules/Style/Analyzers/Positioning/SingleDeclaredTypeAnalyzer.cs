﻿using System.Collections.Immutable;
using System.Composition;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace CodeQuality.Analyzers.Modules.Style.Analyzers.Positioning
{
    [DiagnosticAnalyzer(LanguageNames.CSharp), Shared]
    // ReSharper disable once UnusedMember.Global
    public class SingleDeclaredTypeAnalyzer : DiagnosticAnalyzer
    {
        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics
            => ImmutableArray.Create(Descriptors.CQ006TypesInFile);

        public override void Initialize(AnalysisContext context)
        {
            context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.Analyze | GeneratedCodeAnalysisFlags.ReportDiagnostics);
            context.EnableConcurrentExecution();
            context.RegisterSyntaxTreeAction(Handle);
        }

        private static void Handle(SyntaxTreeAnalysisContext context)
        {
            var root = context.Tree.GetRoot();

            var extraClasses = root.DescendantNodes()
                .OfType<ClassDeclarationSyntax>()
                .Where(cls => cls.Parent.IsKind(SyntaxKind.NamespaceDeclaration))
                .Skip(1)
                .ToList();

            extraClasses.ForEach(cls => context
                .ReportDiagnostic(Diagnostic.Create(Descriptors.CQ006TypesInFile, cls.Identifier.GetLocation(), cls.Identifier.Text)));
        }


        //public static async Task<Solution> CodeFix(Document document, CodeFixContext context, CancellationToken cancellationToken)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
