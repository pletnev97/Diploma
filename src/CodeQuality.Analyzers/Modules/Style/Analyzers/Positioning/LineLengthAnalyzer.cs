﻿using System.Collections.Immutable;
using System.Composition;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CodeFixes;
using Microsoft.CodeAnalysis.Diagnostics;
using Microsoft.CodeAnalysis.Formatting;

namespace CodeQuality.Analyzers.Modules.Style.Analyzers.Positioning
{
    [DiagnosticAnalyzer(LanguageNames.CSharp), Shared]
    // ReSharper disable once UnusedMember.Global
    public class LineLengthAnalyzer : DiagnosticAnalyzer
    {
        private const int MaxLength = 125;

        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics
            => ImmutableArray.Create(Descriptors.CQ004LongLine);

        public override void Initialize(AnalysisContext context)
        {
            context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.Analyze | GeneratedCodeAnalysisFlags.ReportDiagnostics);
            context.EnableConcurrentExecution();
            context.RegisterSyntaxTreeAction(Handle);
        }

        private static void Handle(SyntaxTreeAnalysisContext context)
        {
            var tree = context.Tree;

            var lines = tree.GetText().Lines;

            foreach (var l in lines)
            {
                var span = l.Span;
                if (span.Length > MaxLength)
                    context.ReportDiagnostic(Diagnostic.Create(Descriptors.CQ004LongLine,
                        Location.Create(tree, span)));
            }
        }

        public static async Task<Solution> CodeFix(Document document, CodeFixContext context,
            CancellationToken cancellationToken)
        {
            var diagnosticSpan = context.Diagnostics.First().Location.SourceSpan;

            var syntaxRoot = (await document.GetSyntaxRootAsync(cancellationToken));

            syntaxRoot = syntaxRoot.ReplaceNodes(syntaxRoot.DescendantNodesAndSelf(diagnosticSpan),
                ((_, syntaxNode) => syntaxNode.WithAdditionalAnnotations(Formatter.Annotation)));

            return document.WithSyntaxRoot(syntaxRoot).Project.Solution;
        }
    }
}