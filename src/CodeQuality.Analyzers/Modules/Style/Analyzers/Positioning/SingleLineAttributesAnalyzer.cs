﻿using System.Collections.Immutable;
using System.Composition;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace CodeQuality.Analyzers.Modules.Style.Analyzers.Positioning
{
    [DiagnosticAnalyzer(LanguageNames.CSharp), Shared]
    // ReSharper disable once UnusedMember.Global
    public class SingleLineAttributesAnalyzer : DiagnosticAnalyzer
    {
        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics
            => ImmutableArray.Create(Descriptors.CQ007AttributesPosition);

        public override void Initialize(AnalysisContext context)
        {
            context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.Analyze | GeneratedCodeAnalysisFlags.ReportDiagnostics);
            context.EnableConcurrentExecution();
            context.RegisterSyntaxTreeAction(Handle);
        }

        private static void Handle(SyntaxTreeAnalysisContext context)
        {
            var tree = context.Tree;

            if (!tree.HasCompilationUnitRoot) return;

            var root = tree.GetRoot();

            var singleLineAttrsGrouped = root.DescendantNodes()
                .OfType<AttributeListSyntax>()
                .Select(attr => new { Attribute = attr, StartLine = tree.GetLineSpan(attr.Span).StartLinePosition.Line })
                .GroupBy(x => x.StartLine)
                .Where(gr => gr.Count() > 1);

            foreach (var grouped in singleLineAttrsGrouped)
            {
                foreach (var item in grouped)
                {
                    context.ReportDiagnostic(Diagnostic.Create(Descriptors.CQ007AttributesPosition, item.Attribute.GetLocation()));
                }
            }
        }

        //public static async Task<Document> CodeFix(Document document, CodeFixContext context,
        //    CancellationToken cancellationToken)
        //{
        //    var root = await context.Document.GetSyntaxRootAsync(context.CancellationToken);

        //    var lineAttrs = root.FindNode(context.Diagnostics.First().Location.SourceSpan)
        //        .Parent
        //        .ChildNodes()
        //        .OfType<AttributeListSyntax>()
        //        .ToList();

        //    var formattedRoot = root.ReplaceNodes(lineAttrs,
        //        (_, node) => node
        //            .WithAdditionalAnnotations(Formatter.Annotation));

        //    return document.WithSyntaxRoot(formattedRoot);
        //}
    }
}
