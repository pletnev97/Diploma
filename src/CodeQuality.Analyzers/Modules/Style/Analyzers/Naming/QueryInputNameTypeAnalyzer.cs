﻿using System;
using System.Collections.Immutable;
using System.Composition;
using System.Linq;
using System.Text.RegularExpressions;
using CodeQuality.Analyzers.Helpers;
using CodeQuality.Analyzers.Helpers.KnownSymbols;
using Gu.Roslyn.AnalyzerExtensions;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace CodeQuality.Analyzers.Modules.Style.Analyzers.Naming
{
    namespace CodeQuality.Analyzers.Modules.Style.Analyzers.Naming
    {
        [DiagnosticAnalyzer(LanguageNames.CSharp), Shared]
        // ReSharper disable once UnusedMember.Global
        public class QueryInputNameTypeAnalyzer : DiagnosticAnalyzer
        {
            public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics
                => ImmutableArray.Create(Descriptors.CQ017QueryInputParameter);

            public override void Initialize(AnalysisContext context)
            {
                context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.Analyze | GeneratedCodeAnalysisFlags.ReportDiagnostics);
                context.EnableConcurrentExecution();
                context.RegisterSyntaxNodeAction(Handle, SyntaxKind.ClassDeclaration);
            }

            private static void Handle(SyntaxNodeAnalysisContext context)
            {
                var regex = new Regex("^Get(.)*(Items|Details)$");

                if (!context.IsExcludedFromAnalysis() && context.Node is ClassDeclarationSyntax classNode &&
                    context.SemanticModel.TryGetType(classNode, context.CancellationToken, out var classSymbol) &&
                    classSymbol.TryFindInterface(KnownSymbol.GenericQueryInterface, context.Compilation, out _) &&
                    classSymbol.GetMembers().OfType<IMethodSymbol>()
                        .TryFirst(m => m == KnownSymbol.GenericQueryInterface.Ask, out var askMethodSymbol) &&
                    askMethodSymbol.Parameters.Count() == 1 &&
                    askMethodSymbol.Parameters.TryFirst(out var inputDtoSymbol) &&
                    !regex.IsMatch(inputDtoSymbol.Type.Name))
                {
                    context.ReportDiagnostic(Diagnostic.Create(Descriptors.CQ017QueryInputParameter,
                        FindMethodParameter(context, askMethodSymbol, inputDtoSymbol).GetLocation()));
                }
            }

            private static TypeSyntax FindMethodParameter(SyntaxNodeAnalysisContext context,
                IMethodSymbol methodOwnerSymbol, IParameterSymbol parameterSymbol)
            {
                return ((ClassDeclarationSyntax) context.Node)
                    .DescendantNodes()
                    .OfType<MethodDeclarationSyntax>()
                    .First(m => context.SemanticModel
                                    .TryGetSymbol(m, context.CancellationToken, out var symbol) &&
                                SymbolEqualityComparer.Default.Equals(symbol, methodOwnerSymbol) &&
                                SymbolEqualityComparer.Default.Equals(methodOwnerSymbol.Parameters.First(),
                                    parameterSymbol))
                    .ParameterList.Find(parameterSymbol.Name)?.Type ?? throw new InvalidOperationException("Unexpected error.");
            }
        }
    }
}