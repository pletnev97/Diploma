﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Composition;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CodeFixes;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;
using Microsoft.CodeAnalysis.Rename;

namespace CodeQuality.Analyzers.Modules.Style.Analyzers.Naming
{
    [DiagnosticAnalyzer(LanguageNames.CSharp), Shared]
    // ReSharper disable once UnusedMember.Global
    public class BoolVariableAnalyzer : DiagnosticAnalyzer
    {
        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics
            => ImmutableArray.Create(Descriptors.CQ003BooleanVariable);

        private static IEnumerable<string> AvailablePrefixes => new List<string> {"is"};

        public override void Initialize(AnalysisContext context)
        {
            context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.Analyze | GeneratedCodeAnalysisFlags.ReportDiagnostics);
            context.EnableConcurrentExecution();
            context.RegisterSemanticModelAction(Handle);
        }

        private static void Handle(SemanticModelAnalysisContext context)
        {
            var semanticModel = context.SemanticModel;

            var variableDeclarations = semanticModel.SyntaxTree
                .GetRoot()
                .DescendantNodes()
                .OfType<VariableDeclarationSyntax>()
                .Where(node =>
                    node.Ancestors().OfType<LocalDeclarationStatementSyntax>().Any()
                );

            foreach (var declaration in variableDeclarations)
            {
                var symbolType = semanticModel.GetTypeInfo(declaration.Type).Type;
                if (symbolType.SpecialType != SpecialType.System_Boolean) continue;
                foreach (var variable in declaration.Variables)
                {
                    if (!AvailablePrefixes.Any(ap => variable.Identifier.Text.StartsWith(ap)))
                    {
                        context.ReportDiagnostic(Diagnostic.Create(Descriptors.CQ003BooleanVariable,
                            variable.Identifier.GetLocation()));
                    }
                }
            }
        }

        public static async Task<Solution> CodeFix(Document document, CodeFixContext context,
            CancellationToken cancellationToken)
        {
            var root = await context.Document.GetSyntaxRootAsync(context.CancellationToken)
                .ConfigureAwait(false);

            var diagnostic = context.Diagnostics.First();
            var diagnosticSpan = diagnostic.Location.SourceSpan;

            var localDecl = (VariableDeclaratorSyntax) root.FindNode(diagnosticSpan);

            var identifierTokenName = localDecl.Identifier.Text;

            var newName = "is" + char.ToUpperInvariant(identifierTokenName[0])
                               + identifierTokenName.Substring(1, identifierTokenName.Length - 1);

            var semanticModel = await document.GetSemanticModelAsync(cancellationToken);
            var typeSymbol = ModelExtensions.GetDeclaredSymbol(semanticModel, localDecl, cancellationToken);

            var originalSolution = document.Project.Solution;
            var optionSet = originalSolution.Workspace.Options;
            var newSolution = await Renamer
                .RenameSymbolAsync(document.Project.Solution, typeSymbol, newName, optionSet, cancellationToken)
                .ConfigureAwait(false);

            return newSolution;
        }
    }
}