﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Composition;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace CodeQuality.Analyzers.Modules.Style.Analyzers.Naming
{
    [DiagnosticAnalyzer(LanguageNames.CSharp), Shared]
    // ReSharper disable once UnusedMember.Global
    public class FluentInterfaceAnalyzer : DiagnosticAnalyzer
    {
        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics
            => ImmutableArray.Create(Descriptors.CQ024FluentInterface);

        public override void Initialize(AnalysisContext context)
        {
            context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.Analyze |
                                                   GeneratedCodeAnalysisFlags.ReportDiagnostics);
            context.EnableConcurrentExecution();
            context.RegisterSyntaxNodeAction(Handle, SyntaxKind.SimpleMemberAccessExpression);
        }

        private static void Handle(SyntaxNodeAnalysisContext context)
        {
            var expression = context.Node as MemberAccessExpressionSyntax;
            var dotToken = expression
                .ChildTokens()
                .FirstOrDefault(t => t.IsKind(SyntaxKind.DotToken));

            if (expression.ChildNodes().Any(n => n.IsKind(SyntaxKind.InvocationExpression)) &&
                dotToken != null &&
                !dotToken.LeadingTrivia.Any(t => t.IsKind(SyntaxKind.WhitespaceTrivia)))
            {
                context.ReportDiagnostic(Diagnostic.Create(Descriptors.CQ024FluentInterface,
                    expression.GetLocation(),
                    expression.GetText()));
            }
        }
    }
}