﻿using System.Collections.Immutable;
using System.Composition;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CodeFixes;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;
using Microsoft.CodeAnalysis.Rename;

namespace CodeQuality.Analyzers.Modules.Style.Analyzers.Naming
{
    [DiagnosticAnalyzer(LanguageNames.CSharp), Shared]
    // ReSharper disable once UnusedMember.Global
    public class SimpleInterfaceAnalyzer : DiagnosticAnalyzer
    {
        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics
            => ImmutableArray.Create(Descriptors.CQ005SimpleInterfaceName);

        public override void Initialize(AnalysisContext context)
        {
            context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.Analyze | GeneratedCodeAnalysisFlags.ReportDiagnostics);
            context.EnableConcurrentExecution();
            context.RegisterSyntaxNodeAction(Handle, SyntaxKind.InterfaceDeclaration);
        }

        private static void Handle(SyntaxNodeAnalysisContext context)
        {
            var interfaceDeclaration = (InterfaceDeclarationSyntax)context.Node;

            if (interfaceDeclaration
                .ChildNodes()
                .Count(item => item.IsKind(SyntaxKind.PropertyDeclaration)) != 1)
                return;

            var interfaceName = interfaceDeclaration
                .ChildTokens()
                .First(t => t.IsKind(SyntaxKind.IdentifierToken));

            var singlePropName = interfaceDeclaration.ChildNodes()
                .First(n => n.IsKind(SyntaxKind.PropertyDeclaration))
                .ChildTokens()
                .First(ct => ct.IsKind(SyntaxKind.IdentifierToken))
                .Text;

            if (interfaceName.Text == "IHas" + singlePropName)
                return;

            var diagnostic = Diagnostic.Create(Descriptors.CQ005SimpleInterfaceName, interfaceName.GetLocation(), interfaceDeclaration.GetText());
            context.ReportDiagnostic(diagnostic);
        }

        public static async Task<Solution> CodeFix(Document document, CodeFixContext context,
            CancellationToken cancellationToken)
        {
            var diagnostic = context.Diagnostics.First();
            var diagnosticSpan = diagnostic.Location.SourceSpan;

            var semanticModel = await document.GetSemanticModelAsync(cancellationToken);

            var root = await document.GetSyntaxRootAsync(cancellationToken);

            var symbol = semanticModel.GetDeclaredSymbol(root.FindNode(diagnosticSpan));

            var singlePropName = root.FindNode(diagnosticSpan)
                .ChildNodes()
                .First(n => n.IsKind(SyntaxKind.PropertyDeclaration))
                .ChildTokens()
                .First(ct => ct.IsKind(SyntaxKind.IdentifierToken))
                .Text;

            var optionSet = document.Project.Solution.Workspace.Options;

            return await Renamer.RenameSymbolAsync(document.Project.Solution,
                symbol,
                $"IHas{singlePropName}",
                optionSet,
                cancellationToken);
        }
    }
}
