﻿using System;
using System.Collections.Immutable;
using System.Composition;
using System.Linq;
using CodeQuality.Analyzers.Modules.Style.Helpers;
using Gu.Roslyn.AnalyzerExtensions;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace CodeQuality.Analyzers.Modules.Style.Analyzers.Naming
{
    [DiagnosticAnalyzer(LanguageNames.CSharp), Shared]
    // ReSharper disable once UnusedMember.Global
    public class CommandMethodInputTypeNameAnalyzer : DiagnosticAnalyzer
    {
        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics
            => ImmutableArray.Create(Descriptors.CQ010GetCommandInputDtoName);

        public override void Initialize(AnalysisContext context)
        {
            context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.Analyze | GeneratedCodeAnalysisFlags.ReportDiagnostics);
            context.EnableConcurrentExecution();
            context.RegisterSyntaxNodeAction(Handle, SyntaxKind.MethodDeclaration);
        }

        private static void Handle(SyntaxNodeAnalysisContext context)
        {
            if (context.IsExcludedFromAnalysis() || !(context.Node is MethodDeclarationSyntax node) ||
                !node.AttributeLists.Any() ||
                node.ParameterList.Parameters.Count != 1 || !(node.ParameterList.Parameters.First() is { } parameter) ||
                parameter.Type == null) return;

            var restAttributesTypes = FindHttpRestAttributesWalker
                .Visit(node, context.SemanticModel, context.CancellationToken)
                .GetMethodRelatedAttributes(node)
                .Select(attr => context.SemanticModel.GetTypeInfo(attr).Type)
                .Where(type => type != null)
                .ToArray();

            var paramType = context.SemanticModel.GetTypeInfo(parameter.Type).Type;

            if (restAttributesTypes.Any() &&
                (!CheckParameterHasCorrectPrefix(paramType, restAttributesTypes.First()) ||
                 !CheckParameterHasCorrectPostfix(paramType)))
            {
                context.ReportDiagnostic(Diagnostic.Create(Descriptors.CQ010GetCommandInputDtoName,
                    parameter.Type.GetLocation()));
            }
        }

        private static bool CheckParameterHasCorrectPrefix(ISymbol paramType, ISymbol attributeType) =>
            attributeType.Name switch
            {
                "HttpPostAttribute" => paramType.Name.StartsWith("Create"),
                "HttpPutAttribute" => paramType.Name.StartsWith("Update"),
                "HttpDeleteAttribute" => paramType.Name.StartsWith("Delete"),
                _ => false
            };

        private static bool CheckParameterHasCorrectPostfix(ISymbol paramType) =>
            !paramType.Name.EndsWith("Dto", StringComparison.CurrentCultureIgnoreCase);
    }
}