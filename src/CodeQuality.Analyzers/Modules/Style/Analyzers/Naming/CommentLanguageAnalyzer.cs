﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using CodeQuality.Analyzers.Modules.Style.Helpers;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace CodeQuality.Analyzers.Modules.Style.Analyzers.Naming
{
    [DiagnosticAnalyzer(LanguageNames.CSharp)]
    public class CommentLanguageAnalyzer : DiagnosticAnalyzer
    {
        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics
            => ImmutableArray.Create(Descriptors.CQ022RussianComments, Descriptors.CQ023EnglishComments);

        private const bool IsRussian = false;

        public override void Initialize(AnalysisContext context)
        {
            context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.Analyze |
                                                   GeneratedCodeAnalysisFlags.ReportDiagnostics);
            context.EnableConcurrentExecution();
            context.RegisterSyntaxNodeAction(HandleXml, SyntaxKind.SingleLineDocumentationCommentTrivia);
            context.RegisterSyntaxTreeAction(Handle);
        }

        private static void HandleXml(SyntaxNodeAnalysisContext context)
        {
            var comment = context.Node as DocumentationCommentTriviaSyntax;

            var comments = comment
                .DescendantTokens()
                .Where(t => t.IsKind(SyntaxKind.XmlTextLiteralToken))
                .Select(t => new Comment(t));

            var commentContext = new CommentContext(context);
            Report(commentContext, comments);
        }

        private static void Handle(SyntaxTreeAnalysisContext context)
        {
            var root = context.Tree.GetRoot();

            var comments = root.DescendantTrivia()
                .Where(t =>
                t.IsKind(SyntaxKind.SingleLineCommentTrivia) ||
                t.IsKind(SyntaxKind.MultiLineCommentTrivia))
                .Select(t =>
                {
                    var type = CommentType.None;
                    if (t.IsKind(SyntaxKind.SingleLineCommentTrivia)) type = CommentType.Single;
                    else if (t.IsKind(SyntaxKind.MultiLineCommentTrivia)) type = CommentType.Multi;
                    return new Comment(t, type);
                });

            var commentContext = new CommentContext(context);
            Report(commentContext, comments);
        }

        private static void Report(CommentContext context, IEnumerable<Comment> comments)
        {
            foreach (var c in comments)
            {
                var text = c.GetText;
                var location = c.GetLocation;
                if (IsRussian)
                {
                    if (text.IsNotRussian())
                        context.ReportDiagnostic(Diagnostic.Create(Descriptors.CQ022RussianComments, location));
                }
                else if(text.IsNotEnglish())
                    context.ReportDiagnostic(Diagnostic.Create(Descriptors.CQ023EnglishComments, location));
            }
        }
    }
}