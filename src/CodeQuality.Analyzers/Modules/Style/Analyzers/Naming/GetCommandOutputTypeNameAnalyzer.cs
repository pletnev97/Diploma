﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using CodeQuality.Analyzers.Helpers.KnownSymbols;
using CodeQuality.Analyzers.Modules.Style.Helpers;
using Gu.Roslyn.AnalyzerExtensions;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace CodeQuality.Analyzers.Modules.Style.Analyzers.Naming
{
    [DiagnosticAnalyzer(LanguageNames.CSharp)]
    public class GetCommandOutputTypeNameAnalyzer : DiagnosticAnalyzer
    {
        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics
            => ImmutableArray.Create(Descriptors.CQ011GetMethodResultDtoName);

        public override void Initialize(AnalysisContext context)
        {
            context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.Analyze |
                                                   GeneratedCodeAnalysisFlags.ReportDiagnostics);
            context.EnableConcurrentExecution();
            context.RegisterSyntaxNodeAction(Handle, SyntaxKind.MethodDeclaration);
        }

        private static void Handle(SyntaxNodeAnalysisContext context)
        {
            if (!context.IsExcludedFromAnalysis() &&
                context.Node is MethodDeclarationSyntax { ReturnType: { } returnTypeNode } methodNode &&
                returnTypeNode is GenericNameSyntax genericNodeType &&
                context.SemanticModel.TryGetType(returnTypeNode, context.CancellationToken, out var returnType) &&
                returnType.TryGetSingleTypeArgument(out var genericTypeSymbol) &&
                context.SemanticModel.GetDeclaredSymbol(methodNode) is { } methodSymbol &&
                (methodSymbol == KnownSymbol.GenericQueryInterface.Ask ||
                 FindHttpRestAttributesWalker.Visit(methodNode, context.SemanticModel, context.CancellationToken)
                     .GetMethodRelatedAttributes(methodNode)
                     .Select(attr => ModelExtensions.GetTypeInfo(context.SemanticModel, attr).Type)
                     .Any(type =>
                         type != null && type.IsAssignableTo(KnownSymbol.HttpGetAttribute, context.Compilation))))
            {
                var isCollectionGeneric = ((INamedTypeSymbol) genericTypeSymbol).IsGenericType
                                          && genericTypeSymbol.AllInterfaces.Any(i =>
                                              i.IsAssignableTo(KnownSymbol.EnumerableInterface, context.Compilation));

                var typeNode = GenericArgumentWalker
                    .Visit(genericNodeType,
                        context.SemanticModel)
                    .GetArgumentBySymbol(genericTypeSymbol) ?? throw new InvalidOperationException();

                var resultNode = isCollectionGeneric
                    ? ((GenericNameSyntax) typeNode).TypeArgumentList.Arguments.First()
                    : typeNode;

                var resultTypeSymbol = ModelExtensions.GetTypeInfo(context.SemanticModel, resultNode).Type;

                if (resultTypeSymbol != null && !(isCollectionGeneric && resultTypeSymbol.Name.EndsWith("Item") ||
                                                  resultTypeSymbol.Name.EndsWith("Details")))
                {
                    context.ReportDiagnostic(Diagnostic.Create(Descriptors.CQ011GetMethodResultDtoName,
                        typeNode.GetLocation()));
                }
            }
        }

        public class GenericArgumentWalker : ExecutionWalker<GenericArgumentWalker>
        {
            private readonly SemanticModel _semanticModel;

            private readonly List<TypeSyntax> _genericArguments;

            private GenericArgumentWalker(SemanticModel semanticModel)
            {
                _semanticModel = semanticModel;
                _genericArguments = new List<TypeSyntax>();
            }

            public override void VisitTypeArgumentList(TypeArgumentListSyntax node) =>
                _genericArguments.AddRange(node.Arguments);

            public static GenericArgumentWalker Visit(SyntaxNode node, SemanticModel semanticModel) =>
                BorrowAndVisit(node, () => new GenericArgumentWalker(semanticModel));

            public TypeSyntax? GetArgumentBySymbol(ITypeSymbol typeSymbol) => _genericArguments
                .Select(ga => new {Node = ga, _semanticModel.GetTypeInfo(ga).Type})
                .Where(agg => agg.Type != null)
                .FirstOrDefault(agg => agg.Type.IsAssignableTo(typeSymbol, _semanticModel.Compilation))?.Node;
        }
    }
}