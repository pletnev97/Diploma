﻿using System.Collections.Immutable;
using System.Composition;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CodeFixes;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace CodeQuality.Analyzers.Modules.Style.Analyzers.Naming
{
    [DiagnosticAnalyzer(LanguageNames.CSharp), Shared]
    // ReSharper disable once UnusedMember.Global
    public class BoolPropertyAnalyzer : DiagnosticAnalyzer
    {
        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics
            => ImmutableArray.Create(Descriptors.CQ002BooleanProperty);

        public override void Initialize(AnalysisContext context)
        {
            context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.Analyze | GeneratedCodeAnalysisFlags.ReportDiagnostics);
            context.EnableConcurrentExecution();
            context.RegisterSyntaxNodeAction(Handle, SyntaxKind.PropertyDeclaration);
        }

        private static void Handle(SyntaxNodeAnalysisContext context)
        {
            var property = (PropertyDeclarationSyntax) context.Node;

            var isBoolProperty = property
                .ChildNodes()
                .OfType<PredefinedTypeSyntax>()
                .FirstOrDefault()?
                .ChildTokens()
                .Any(t => t.IsKind(SyntaxKind.BoolKeyword)) ?? false;

            if (!isBoolProperty) return;

            if (!property.Identifier.Text.StartsWith("Is"))
            {
                context.ReportDiagnostic(Diagnostic
                    .Create(Descriptors.CQ002BooleanProperty, property.Identifier.GetLocation()));
            }
        }

        public static async Task<Document> CodeFix(Document document, CodeFixContext context,
            CancellationToken cancellationToken)
        {
            var root = await context.Document.GetSyntaxRootAsync(context.CancellationToken)
                .ConfigureAwait(false);

            var diagnosticSpan = context.Diagnostics.First().Location.SourceSpan;

            var propertyDeclaration = (PropertyDeclarationSyntax) root.FindNode(diagnosticSpan);

            var identifierTokenName = propertyDeclaration.Identifier.Text;

            var newName = "Is" + char.ToUpperInvariant(identifierTokenName[0])
                               + identifierTokenName.Substring(1, identifierTokenName.Length - 1);

            var leadingTrivia = propertyDeclaration.Identifier.LeadingTrivia;
            var trailingTrivia = propertyDeclaration.Identifier.TrailingTrivia;

            var modifiedProp = propertyDeclaration
                .WithIdentifier(SyntaxFactory.Identifier(leadingTrivia, newName, trailingTrivia));

            var newRoot = root.ReplaceNode(propertyDeclaration, modifiedProp);

            return document.WithSyntaxRoot(newRoot);
        }
    }
}