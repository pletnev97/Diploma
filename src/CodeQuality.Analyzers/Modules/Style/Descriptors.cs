﻿using System.Diagnostics.CodeAnalysis;
using CodeQuality.Analyzers.Helpers;
using Microsoft.CodeAnalysis;

namespace CodeQuality.Analyzers.Modules.Style
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public static class Descriptors
    {
        public static readonly DiagnosticDescriptor CQ002BooleanProperty = DiagnosticDescriptorHelper.Create(
            "CQ002",
            "Bool property has incorrect name.",
            @"Bool property should have ""is"" prefix.",
            AnalyzerCategory.Style,
            DiagnosticSeverity.Error,
            true,
            @"Bool property should have ""Is"" prefix.");

        public static readonly DiagnosticDescriptor CQ003BooleanVariable = DiagnosticDescriptorHelper.Create(
            "CQ003",
            "Bool local variable has incorrect name.",
            @"local variable should have ""is"" prefix.",
            AnalyzerCategory.Style,
            DiagnosticSeverity.Warning,
            true,
            @"local variable should have ""is"" prefix.");

        public static readonly DiagnosticDescriptor CQ004LongLine = DiagnosticDescriptorHelper.Create(
            "CQ004",
            "Line is too long.",
            "Line should be shorter than 80 symbols.",
            AnalyzerCategory.Style,
            DiagnosticSeverity.Warning,
            true,
            "Line should be shorter than 80 symbols.");

        public static readonly DiagnosticDescriptor CQ005SimpleInterfaceName = DiagnosticDescriptorHelper.Create(
            "CQ005",
            "Rename interface with IHas prefix.",
            "Rename interface with IHas prefix.",
            AnalyzerCategory.Style,
            DiagnosticSeverity.Warning,
            true,
            "Rename interface with IHas prefix.");

        public static readonly DiagnosticDescriptor CQ006TypesInFile = DiagnosticDescriptorHelper.Create(
            "CQ006",
            "More than one class in source file.",
            "Only one class is available in source file.",
            AnalyzerCategory.Style,
            DiagnosticSeverity.Warning,
            true,
            "Only one class is available in source file.");

        public static readonly DiagnosticDescriptor CQ007AttributesPosition = DiagnosticDescriptorHelper.Create(
            "CQ007",
            "Attributes positioned on one line.",
            "Attributes should be positioned on own line.",
            AnalyzerCategory.Style,
            DiagnosticSeverity.Warning,
            true,
            "Attributes should be positioned on own line.");

        public static readonly DiagnosticDescriptor CQ010GetCommandInputDtoName = DiagnosticDescriptorHelper.Create(
            "CQ010",
            "Incorrect command class name.",
            "The class name does not match the pattern \"{CommandName}{EntityName}\".",
            AnalyzerCategory.Style,
            DiagnosticSeverity.Warning,
            true,
            "The class name does not match the pattern \"{EntityName}{EntityName}\".");

        public static readonly DiagnosticDescriptor CQ011GetMethodResultDtoName = DiagnosticDescriptorHelper.Create(
            "CQ011",
            "Incorrect result class name.",
            "The class name does not match the pattern \"{CommandName}{EntityName}\".",
            AnalyzerCategory.Style,
            DiagnosticSeverity.Warning,
            true,
            "The class name does not match the pattern \"{EntityName}{Details | Item}\".");

        public static readonly DiagnosticDescriptor CQ017QueryInputParameter = DiagnosticDescriptorHelper.Create(
            "CQ017",
            "Incorrect type name.",
            "The type name does not match the pattern \"Get{EntityName}(Items | Details).",
            AnalyzerCategory.Style,
            DiagnosticSeverity.Warning,
            true,
            "The type name does not match the pattern \"Get{EntityName}(Items | Details).");

        public static readonly DiagnosticDescriptor CQ022RussianComments = DiagnosticDescriptorHelper.Create(
            "CQ022",
            "Comments have wrong languages.",
            "Comments must be in russian.",
            AnalyzerCategory.Style,
            DiagnosticSeverity.Warning,
            true,
            "Comments must be in russian.");

        public static readonly DiagnosticDescriptor CQ023EnglishComments = DiagnosticDescriptorHelper.Create(
            "CQ023",
            "Comment has wrong language.",
            "Comments must be in english.",
            AnalyzerCategory.Style,
            DiagnosticSeverity.Warning,
            true,
            "Comments must be in english.");

        public static readonly DiagnosticDescriptor CQ024FluentInterface = DiagnosticDescriptorHelper.Create(
            "CQ024",
            "Fluent-chain has wrong format.",
            "Fluent-chain must be start with new line.",
            AnalyzerCategory.Style,
            DiagnosticSeverity.Warning,
            true,
            "Fluent-chain must be start with new line.");
    }
}
