﻿namespace CodeQuality.Analyzers.Modules.Style.Helpers
{
    public enum CommandAttributeType
    {
        Create,
        Update,
        Delete
    }
}
