﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Diagnostics;

namespace CodeQuality.Analyzers.Modules.Style.Helpers
{
    internal class CommentContext
    {
        private readonly SyntaxNodeAnalysisContext nodeContext;
        private readonly SyntaxTreeAnalysisContext treeContext;
        private readonly bool IsNode;

        internal CommentContext(SyntaxNodeAnalysisContext сontext)
        {
            nodeContext = сontext;
            IsNode = true;
        }

        internal CommentContext(SyntaxTreeAnalysisContext сontext)
        {
            treeContext = сontext;
            IsNode = false;
        }

        internal void ReportDiagnostic(Diagnostic diagnostic)
        {
            if (IsNode) nodeContext.ReportDiagnostic(diagnostic);
            else treeContext.ReportDiagnostic(diagnostic);
        }
    }
}
