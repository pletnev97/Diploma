﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using CodeQuality.Analyzers.Helpers.KnownSymbols;
using Gu.Roslyn.AnalyzerExtensions;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace CodeQuality.Analyzers.Modules.Style.Helpers
{
    public class FindHttpRestAttributesWalker : ExecutionWalker<FindHttpRestAttributesWalker>
    {
        public IList<AttributeSyntax> Attributes { get; }

        private readonly SemanticModel _semanticModel;

        private FindHttpRestAttributesWalker(SemanticModel semanticModel)
        {
            _semanticModel = semanticModel;
            Attributes = new List<AttributeSyntax>();
        }

        public static FindHttpRestAttributesWalker Visit(MethodDeclarationSyntax node, SemanticModel semanticModel,
            CancellationToken cancellationToken)
        {
            return BorrowAndVisit(node, () => new FindHttpRestAttributesWalker(semanticModel));
        }

        public override void VisitAttribute(AttributeSyntax node)
        {
            if (
                _semanticModel.TryGetType(node, default, out var typeSymbol) &&
                (typeSymbol.IsAssignableTo(KnownSymbol.HttpPostAttribute, _semanticModel.Compilation) ||
                 typeSymbol.IsAssignableTo(KnownSymbol.HttpGetAttribute, _semanticModel.Compilation) ||
                 typeSymbol.IsAssignableTo(KnownSymbol.HttpPutAttribute, _semanticModel.Compilation) ||
                 typeSymbol.IsAssignableTo(KnownSymbol.HttpDeleteAttribute, _semanticModel.Compilation)))
            {
                Attributes.Add(node);
            }
        }

        public IList<AttributeSyntax> GetMethodRelatedAttributes(MethodDeclarationSyntax node) =>
            node.AttributeLists
                .SelectMany(al => al.Attributes)
                .Intersect(Attributes)
                .ToList();
    }
}