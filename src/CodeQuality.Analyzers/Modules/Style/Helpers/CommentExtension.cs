﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeQuality.Analyzers.Modules.Style.Helpers
{
    internal static class CommentExtention
    {
        internal static bool IsRussian(this char c)
        {
            return c >= 1040 && c <= 1103 || c == 1025 || c == 1105;
        }

        internal static bool IsNotEnglish(this string str)
        {
            return str.Any(c => c.IsRussian());
        }

        internal static bool IsNotRussian(this string str)
        {
            if (str == " ") return false;
            var russianSymbCount = str
                .Where(c => c.IsRussian())
                .Count();
            return 2 * russianSymbCount <= str.Length;
        }
    }
}