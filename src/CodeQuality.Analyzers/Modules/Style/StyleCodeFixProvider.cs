using System;
using System.Collections.Immutable;
using System.Composition;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CodeQuality.Analyzers.Helpers;
using CodeQuality.Analyzers.Helpers.KnownSymbols;
using Gu.Roslyn.AnalyzerExtensions;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CodeActions;
using Microsoft.CodeAnalysis.CodeFixes;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Formatting;
using Microsoft.CodeAnalysis.Rename;

namespace CodeQuality.Analyzers.Modules.Style
{
    [ExportCodeFixProvider(LanguageNames.CSharp, Name = nameof(StyleCodeFixProvider)), Shared]
    public class StyleCodeFixProvider : CodeFixProvider
    {
        public sealed override ImmutableArray<string> FixableDiagnosticIds =>
            ImmutableArray.Create(
                Descriptors.CQ003BooleanVariable.Id,
                Descriptors.CQ002BooleanProperty.Id,
                Descriptors.CQ005SimpleInterfaceName.Id,
                Descriptors.CQ004LongLine.Id,
                Descriptors.CQ006TypesInFile.Id,
                Descriptors.CQ007AttributesPosition.Id);


        public sealed override async Task RegisterCodeFixesAsync(CodeFixContext context)
        {
            var syntaxRoot = await context.Document.GetSyntaxRootAsync(context.CancellationToken)
                .ConfigureAwait(false);

            foreach (var diagnostic in context.Diagnostics)
            {
                if (diagnostic.Id == Descriptors.CQ003BooleanVariable.Id)
                    context.RegisterCodeFix(
                        CodeAction.Create(
                            "Add prefix \"is\".",
                            c => FixBoolVariable(context.Document, context, c),
                            "Add prefix \"is\"."),
                        diagnostic);
                else if (diagnostic.Id == Descriptors.CQ002BooleanProperty.Id)
                    context.RegisterCodeFix(
                        CodeAction.Create(
                            "Add prefix \"Is\".",
                            c => FixBoolProperty(context.Document, context, c),
                            "Add prefix \"Is\"."),
                        diagnostic);
                else if (diagnostic.Id == Descriptors.CQ005SimpleInterfaceName.Id)
                    context.RegisterCodeFix(
                        CodeAction.Create(
                            "Add \"IHas\" prefix.",
                            c => FixInterfaceName(context.Document, context, c),
                            "Add \"IHas\" prefix."),
                        diagnostic);
                else if (diagnostic.Id == Descriptors.CQ004LongLine.Id)
                    context.RegisterCodeFix(
                        CodeAction.Create(
                            "Make the line shorter.",
                            c => FixLongLine(context.Document, context, c),
                            "Make the line shorter."),
                        diagnostic);
                else if (diagnostic.Id == Descriptors.CQ007AttributesPosition.Id)
                    context.RegisterCodeFix(
                        CodeAction.Create(
                            "Break attributes on lines.",
                            c => FixAttributes(context.Document, context, c),
                            "Break attributes on lines."),
                        diagnostic);
                else if (diagnostic.Id == Descriptors.CQ017QueryInputParameter.Id)
                    context.RegisterCodeFix(
                        CodeAction.Create(
                            "Break attributes on lines.",
                            c => FixQueryInputType(context.Document, context, c),
                            "Break attributes on lines."),
                        diagnostic);
            }
        }

        private static async Task<Solution> FixBoolVariable(Document document, CodeFixContext context,
            CancellationToken cancellationToken)
        {
            var root = await context.Document.GetSyntaxRootAsync(context.CancellationToken)
                .ConfigureAwait(false);

            var diagnostic = context.Diagnostics.First();
            var diagnosticSpan = diagnostic.Location.SourceSpan;

            var localDecl = (VariableDeclaratorSyntax) root.FindNode(diagnosticSpan);

            var identifierTokenName = localDecl.Identifier.Text;

            var newName = "is" + char.ToUpperInvariant(identifierTokenName[0])
                               + identifierTokenName.Substring(1, identifierTokenName.Length - 1);

            var semanticModel = await document.GetSemanticModelAsync(cancellationToken);
            var typeSymbol = ModelExtensions.GetDeclaredSymbol(semanticModel, localDecl, cancellationToken);

            var originalSolution = document.Project.Solution;
            var optionSet = originalSolution.Workspace.Options;
            var newSolution = await Renamer
                .RenameSymbolAsync(document.Project.Solution, typeSymbol, newName, optionSet, cancellationToken)
                .ConfigureAwait(false);

            return newSolution;
        }

        private static async Task<Document> FixBoolProperty(Document document, CodeFixContext context,
            CancellationToken cancellationToken)
        {
            var root = await context.Document.GetSyntaxRootAsync(context.CancellationToken)
                .ConfigureAwait(false);

            var diagnosticSpan = context.Diagnostics.First().Location.SourceSpan;

            var propertyDeclaration = (PropertyDeclarationSyntax) root.FindNode(diagnosticSpan);

            var identifierTokenName = propertyDeclaration.Identifier.Text;

            var newName = "Is" + char.ToUpperInvariant(identifierTokenName[0])
                               + identifierTokenName.Substring(1, identifierTokenName.Length - 1);

            var leadingTrivia = propertyDeclaration.Identifier.LeadingTrivia;
            var trailingTrivia = propertyDeclaration.Identifier.TrailingTrivia;

            var modifiedProp = propertyDeclaration
                .WithIdentifier(SyntaxFactory.Identifier(leadingTrivia, newName, trailingTrivia));

            var newRoot = root.ReplaceNode(propertyDeclaration, modifiedProp);

            return document.WithSyntaxRoot(newRoot);
        }

        private static async Task<Solution> FixInterfaceName(Document document, CodeFixContext context,
            CancellationToken cancellationToken)
        {
            var diagnostic = context.Diagnostics.First();
            var diagnosticSpan = diagnostic.Location.SourceSpan;

            var semanticModel = await document.GetSemanticModelAsync(cancellationToken);

            var root = await document.GetSyntaxRootAsync(cancellationToken);

            var symbol = semanticModel.GetDeclaredSymbol(root.FindNode(diagnosticSpan));

            var singlePropName = root.FindNode(diagnosticSpan)
                .ChildNodes()
                .First(n => n.IsKind(SyntaxKind.PropertyDeclaration))
                .ChildTokens()
                .First(ct => ct.IsKind(SyntaxKind.IdentifierToken))
                .Text;

            var optionSet = document.Project.Solution.Workspace.Options;

            return await Renamer.RenameSymbolAsync(document.Project.Solution,
                symbol,
                $"IHas{singlePropName}",
                optionSet,
                cancellationToken);
        }

        private static async Task<Document> FixLongLine(Document document, CodeFixContext context,
            CancellationToken cancellationToken)
        {
            var diagnosticSpan = context.Diagnostics.First().Location.SourceSpan;

            var syntaxRoot = (await document.GetSyntaxRootAsync(cancellationToken));

            syntaxRoot = syntaxRoot.ReplaceNodes(syntaxRoot.DescendantNodesAndSelf(diagnosticSpan),
                ((_, syntaxNode) => syntaxNode.WithAdditionalAnnotations(Formatter.Annotation)));

            return document.WithSyntaxRoot(syntaxRoot);
        }

        private static async Task<Document> FixAttributes(Document document, CodeFixContext context,
            CancellationToken cancellationToken)
        {
            var root = await context.Document.GetSyntaxRootAsync(context.CancellationToken);

            var lineAttrs = root.FindNode(context.Diagnostics.First().Location.SourceSpan)
                .Parent
                .ChildNodes()
                .OfType<AttributeListSyntax>()
                .ToList();

            var formattedRoot = root.ReplaceNodes(lineAttrs,
                (_, node) => node
                    .WithAdditionalAnnotations(Formatter.Annotation));

            return document.WithSyntaxRoot(formattedRoot);
        }

        private static async Task<Solution> FixQueryInputType(Document document, CodeFixContext context,
            CancellationToken cancellationToken)
        {
            var root = await context.Document.GetSyntaxRootAsync(context.CancellationToken);
            var semanticMode = await document.GetSemanticModelAsync(cancellationToken);

            var isSuccess = semanticMode.TryGetSymbol(
                (IdentifierNameSyntax) root.FindNode(context.Diagnostics.First().Location.SourceSpan),
                cancellationToken, out var typeSymbol);

            if (!isSuccess || typeSymbol == null)
                throw new InvalidOperationException();

            var name = string.Copy(typeSymbol.Name);

            if (!name.StartsWith("Get"))
                name = "Get" + name;
            if (name.EndsWith("Dto"))
                name = name.Substring(0, name.LastIndexOf("Dto", StringComparison.Ordinal));

            var returnTypeNode = root
                .FindNode(context.Diagnostics.First().Location.SourceSpan)
                .Ancestors()
                .OfType<MethodDeclarationSyntax>()
                .First()
                .ReturnType;

            semanticMode.TryGetType(returnTypeNode, cancellationToken, out var returnTypeSymbol);

            var isList = returnTypeSymbol != null && returnTypeSymbol.TryFindInterface(KnownSymbol.EnumerableInterface,
                semanticMode.Compilation, out _);

            var postfix = isList ? "Items" : "Details";

            if (name.EndsWith(postfix))
            {
                if (name.EndsWith("Items"))
                    name = name.Substring(0, name.LastIndexOf("Items", StringComparison.Ordinal));
                else if (name.EndsWith("Details"))
                    name = name.Substring(0, name.LastIndexOf("Details", StringComparison.Ordinal));
            }

            name += postfix;

            return await Renamer.RenameSymbolAsync(document.Project.Solution, typeSymbol, name,
                await document.GetOptionsAsync(), cancellationToken);
        }
    }
}